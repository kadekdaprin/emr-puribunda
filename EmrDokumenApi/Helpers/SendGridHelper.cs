﻿//using HaiMedApi.Components;
//using Microsoft.Extensions.Options;
//using SendGrid;
//using SendGrid.Helpers.Mail;
//using System.Threading.Tasks;

//namespace HaiMedApi.Helpers
//{
//    public class SendGridHelper
//    {
//        private readonly SendGridClient client;

//        private readonly EmailAddress from;

//        public EmailContentHelper EmailContentHelper { get; }

//        public SendGridHelper(IOptions<AppSettings> option, EmailContentHelper emailContentHelper)
//        {
//            client = new SendGridClient(option.Value.SendGridApiKey);

//            from = new EmailAddress("haimed@no-reply.com");

//            EmailContentHelper = emailContentHelper;
//        }

//        public Task<Response> SendEmailAsync(
//            EmailAddress to,
//            string subject,
//            string plainContent,
//            string htmlContent
//            )
//        {
//            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainContent, htmlContent);

//            var result = client.SendEmailAsync(msg);

//            return result;
//        }
//    }
//}
