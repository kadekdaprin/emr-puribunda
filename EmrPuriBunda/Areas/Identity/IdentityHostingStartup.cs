using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using EmrPuriBunda.Areas.Identity.Data;
using EmrPuriBunda.Data;
using Microsoft.AspNetCore.Authentication.Cookies;

[assembly: HostingStartup(typeof(EmrPuriBunda.Areas.Identity.IdentityHostingStartup))]
namespace EmrPuriBunda.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) =>
            {
                services.AddDbContext<EmrPuriBundaContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("EmrPuriBundaContextConnection")));

                services.AddDefaultIdentity<ApplicationUser>(options =>
                {
                    options.SignIn.RequireConfirmedAccount = false;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireDigit = false;
                    options.Password.RequiredLength = 2;
                })
                .AddRoles<IdentityRole>()
                    .AddEntityFrameworkStores<EmrPuriBundaContext>();

                //services.ConfigureApplicationCookie(
                //    options =>
                //{
                //    options.AccessDeniedPath = "/Identity/Account/AccessDenied";
                //    options.Cookie.Name = "EmrPuriBunda";
                //    options.Cookie.HttpOnly = true;
                //    options.ExpireTimeSpan = TimeSpan.FromMinutes(60);
                //    options.LoginPath = "/Identity/Account/Login";
                //    // ReturnUrlParameter requires 
                //    //using Microsoft.AspNetCore.Authentication.Cookies;
                //    options.ReturnUrlParameter = CookieAuthenticationDefaults.ReturnUrlParameter;
                //    options.SlidingExpiration = true;
                //});
            });
        }
    }
}