﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using static EmrPuriBunda.Constant.PolicyConstant;
using EmrPuriBunda.Mahas.Helpers;
using Microsoft.Extensions.Options;

namespace EmrPuriBunda.Controllers
{
    [Authorize(Policy = Riwayat)]
    public class PasienController : BaseController
    {
        private readonly IAuthorizationService authService;

        private readonly AppSettings appSettings;

        public PasienController(IConfiguration config, IAuthorizationService authService, IOptions<AppSettings> options) : base(config)
        {
            this.authService = authService;
            appSettings = options.Value;
        }

        public async Task<IActionResult> IndexAsync(string noReg, int nomor, string sectionId = null)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

            var dokter = await GetDokterAsync(userid);

            var errorView = ValidasiHarusDokter(dokter);

            mSectionLogin section;

            try
            {
                section = Request.GetSection();
            }
            catch (Exception)
            {
                return Redirect(Url.Content("~/"));
            }

            if(!string.IsNullOrEmpty(sectionId))
            {
                mSectionLogin sectionLogin;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();

                    var query = $@"
                        SELECT
                            TOP 1
                            SectionID 
                            , SectionName
                            , GroupPelayanan
                            , EResepSectionID
                            , EResepSectionName
                        FROM
                            mSectionLogin
                        WHERE
                            SectionID = @Id
                    ";

                    sectionLogin = await s.GetData<mSectionLogin>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@Id", sectionId),
                    });

                }

                Response.PutSection(sectionLogin);
                section = sectionLogin;
            }

            if (errorView != null) return errorView;

            RegistrasiPasien_DataReg data;
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                var query = $@"
                        SELECT
                            TOP 1 *
                        FROM
                            VW_DataPasienReg
                        WHERE
                            NoReg=@NoReg
                            AND Nomor=@Nomor
                    ";

                data = await s.GetData<RegistrasiPasien_DataReg>(query, new List<SqlParameter>
                {
                    new SqlParameter("@NoReg", noReg),
                    new SqlParameter("@Nomor", nomor),
                });

                ViewData["Pasien"] = data ?? throw new Exception("Data tidak ditemukan");

                try
                {
                    var pengisian = await GetPengisianEMRAsync(noReg, section);
                    ViewData["PengisianEMR"] = pengisian;
                }
                catch (Exception ex)
                {
                    return View("CustomError", new ErrorViewModel()
                    {
                        Errors = null,
                        Title = "Terjadi Kesalahan",
                        SubTitle = ex.Message
                    });
                }

                ViewData["Dokter"] = dokter;
            }

            using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
            {
                var queryKunjunganTerakhir = "SELECT TglReg AS Tanggal FROM SIMtrRegistrasi WHERE Batal = 0 AND NRM = @Nrm and TglReg < @TglReg ORDER BY TglReg DESC";

                var kunjunganTerakhir = await s.GetData<RegistrasiPasien_DataReg>(queryKunjunganTerakhir, new List<SqlParameter>()
                {
                    new SqlParameter("@Nrm", data.NRM),
                    new SqlParameter("@TglReg", data.Tanggal.Date),
                });

                ViewData["TglKunjunganTerakhir"] = kunjunganTerakhir?.Tanggal;
            }

            var menuAkses = await GetMenuAsync(section);

            var result = new RegistrasiPasien_DataRegWithMenu
            {
                Pasien = data,
                MenuAkses = menuAkses
            };

            return View(result);
        }

        [HttpGet]
        [ActionName("ActionPasienRegistrasi")]
        public async Task<JsonResult> Data_PasienRegistrasi(string id)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                RegistrasiPasien_DataReg data;
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var query = $@"
                        SELECT
                            TOP 1 *
                        FROM
                            Vw_PasienRegistrasi
                        WHERE
                            NoReg=@NoReg
                    ";

                    data = await s.GetData<RegistrasiPasien_DataReg>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoReg", id),
                    });

                }

                if (data == null) throw new Exception("Data tidak ditemukan");

                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        public async Task<Pasien_PengisianEMR> GetPengisianEMRAsync(string noReg, mSectionLogin section)
        {
            var tbAsesmenDokter = "";
            var tbAsesmenPerawat = "";

            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                if (appSettings.DefaultConfig.GroupPelayananRJ == section.GroupPelayanan)
                {
                    tbAsesmenDokter = "AsesmenDokterRJ";
                    tbAsesmenPerawat = "AsesmenPerawatRJ";
                }
                else if (appSettings.DefaultConfig.GroupPelayananUGD == section.GroupPelayanan)
                {
                    tbAsesmenDokter = "AsesmenDokterUGD";
                    tbAsesmenPerawat = "AsesmenPerawatUGD";
                }
                else if (appSettings.DefaultConfig.GroupPelayananRI == section.GroupPelayanan)
                {
                    tbAsesmenDokter = "AsesmenDokterRI";
                    tbAsesmenPerawat = "AsesmenPerawatRI";
                }
                else
                {
                    throw new Exception("Section Terpilih, Bukan Section EMR");
                }

                var emrQuery = $@"
                            SELECT 
                            TOP 1
                            NoReg = @NoReg
                            , AsesmenDokter = CAST( CASE WHEN (SELECT COUNT(NoReg) FROM {tbAsesmenDokter} WHERE NoReg = @NoReg) = 0 THEN 0 ELSE 1 END AS BIT)
                            , AsesmenPerawat = CAST( CASE WHEN (SELECT COUNT(NoReg) FROM {tbAsesmenPerawat} WHERE NoReg = @NoReg) = 0 THEN 0 ELSE 1 END AS BIT)
                            , DischargeSummary = CAST( CASE WHEN (SELECT COUNT(NoReg) FROM DischargeSummary WHERE NoReg = @NoReg) = 0 THEN 0 ELSE 1 END AS BIT)";

                var pengisian = await s.GetData<Pasien_PengisianEMR>(emrQuery, new List<SqlParameter> { new SqlParameter("@NoReg", noReg) });

                return pengisian;
            }
        }

        private async Task<List<Pasien_MenuAkses>> GetMenuAsync(mSectionLogin section)
        {
            var allowCppt = await authService.IsInPolicyAsync(User, Cppt);
            var allowEResep = await authService.IsInPolicyAsync(User, EResep);
            var allowEPenunjang = await authService.IsInPolicyAsync(User, EPenunjang);

            var menuList = new List<Pasien_MenuAkses>();

            if (section.GroupPelayanan.Equals(appSettings.DefaultConfig.GroupPelayananUGD))
            {
                var allowAsesmenAwalDokterIGD = await authService.IsInPolicyAsync(User, AsesmenAwalDokterIGD);
                var allowAsesmenAwalPerawatIGD = await authService.IsInPolicyAsync(User, AsesmenAwalPerawatIGD);

                if (allowAsesmenAwalDokterIGD)
                {
                    menuList.Add(new Pasien_MenuAkses()
                    {
                        Title = "Asesmen Awal Dokter",
                        Allow = allowAsesmenAwalDokterIGD,
                        Child = null,
                        Target = "tab_AsesmenAwalDokter",
                        IdFormSetup = "isetup_AsesmenDokterUGD",
                        ViewLocation = "~/Views/AsesmenDokterUGD/SetupAsesmenDokterUGD.cshtml"
                    });
                }

                if (allowAsesmenAwalPerawatIGD)
                {
                    menuList.Add(new Pasien_MenuAkses()
                    {
                        Title = "Asesmen Awal Perawat",
                        Allow = allowAsesmenAwalPerawatIGD,
                        Child = null,
                        Target = "tab_AsesmenAwalPerawat",
                        IdFormSetup = "isetup_AsesmenPerawatUGD",
                        ViewLocation = "~/Views/AsesmenPerawatUGD/SetupAsesmenPerawatUGD.cshtml"
                    });
                }
            }
            else if (section.GroupPelayanan.Equals(appSettings.DefaultConfig.GroupPelayananRJ))
            {
                var allowAsesmenAwalDokterRJ = await authService.IsInPolicyAsync(User, AsesmenAwalDokterRJ);
                var allowAsesmenAwalPerawatRJ = await authService.IsInPolicyAsync(User, AsesmenAwalPerawatRJ);

                if (allowAsesmenAwalDokterRJ)
                {
                    menuList.Add(new Pasien_MenuAkses()
                    {
                        Title = "Asesmen Awal Dokter",
                        Allow = allowAsesmenAwalDokterRJ,
                        Child = null,
                        Target = "tab_AsesmenAwalDokter",
                        IdFormSetup = "isetup_AsesmenDokterRJ",
                        ViewLocation = "~/Views/AsesmenDokterRJ/SetupAsesmenDokterRJ.cshtml"
                    });
                }
                if (allowAsesmenAwalPerawatRJ)
                {
                    menuList.Add(new Pasien_MenuAkses()
                    {
                        Title = "Asesmen Awal Perawat",
                        Allow = allowAsesmenAwalPerawatRJ,
                        Child = null,
                        Target = "tab_AsesmenAwalPerawat",
                        IdFormSetup = "isetup_AsesmenPerawatRJ",
                        ViewLocation = "~/Views/AsesmenPerawatRJ/SetupAsesmenPerawatRJ.cshtml"
                    });
                }
            }
            else if (section.GroupPelayanan.Equals(appSettings.DefaultConfig.GroupPelayananRI))
            {
                var allowAsesmenAwalPerawatRI = await authService.IsInPolicyAsync(User, AsesmenAwalPerawatRI);
                var allowAsesmenAwalDokterRI = await authService.IsInPolicyAsync(User, AsesmenAwalDokterRI);

                if (allowAsesmenAwalDokterRI)
                {
                    menuList.Add(new Pasien_MenuAkses()
                    {
                        Title = "Asesmen Awal Dokter",
                        Allow = allowAsesmenAwalDokterRI,
                        Child = null,
                        Target = "tab_AsesmenAwalDokter",
                        IdFormSetup = "isetup_AsesmenDokterRI",
                        ViewLocation = "~/Views/AsesmenDokterRI/SetupAsesmenDokterRI.cshtml"
                    });
                }

                if (allowAsesmenAwalPerawatRI)
                {
                    menuList.Add(new Pasien_MenuAkses()
                    {
                        Title = "Asesmen Awal Perawat",
                        Allow = allowAsesmenAwalPerawatRI,
                        Child = null,
                        Target = "tab_AsesmenAwalPerawat",
                        IdFormSetup = "isetup_AsesmenPerawatRI",
                        ViewLocation = "~/Views/AsesmenPerawatRI/SetupAsesmenPerawatRI.cshtml"
                    });
                }
            }

            if (allowCppt)
            {
                menuList.Add(new Pasien_MenuAkses()
                {
                    Title = "Cppt",
                    Allow = allowCppt,
                    Child = null,
                    Target = "tab_Cppt",
                    ViewLocation = "~/Views/Cppt/SetupCppt.cshtml"
                });


                if (section.GroupPelayanan.Equals(appSettings.DefaultConfig.GroupPelayananRI) || section.GroupPelayanan.Equals(appSettings.DefaultConfig.GroupPelayananUGD))
                {
                    menuList.Add(new Pasien_MenuAkses()
                    {
                        Title = "Catatan Tindakan",
                        Allow = allowCppt,
                        Child = null,
                        Target = "tab_CatatanKeperawatan",
                        ViewLocation = "~/Views/CatatanKeperawatan/SetupCatatanKeperawatan.cshtml"
                    });
                }
            }

            if (section.GroupPelayanan.Equals(appSettings.DefaultConfig.GroupPelayananRI))
            {
                var allowDischargeSummary = await authService.IsInPolicyAsync(User, DischargeSummary);

                if (allowDischargeSummary)
                {
                    menuList.Add(new Pasien_MenuAkses()
                    {
                        Title = "Discharge Summary",
                        Allow = allowDischargeSummary,
                        Child = null,
                        Target = "tab_DischargeSummary",
                        ViewLocation = "~/Views/DischargeSummary/SetupDischargeSummary.cshtml"
                    });
                }
            }

            menuList.Add(new Pasien_MenuAkses()
            {
                Title = "Order",
                Allow = allowEPenunjang || allowEResep,
                Target = "tab_Order",
                ViewLocation = null,
                Child = new List<Pasien_MenuAkses>() {
                    new Pasien_MenuAkses()
                    {
                        Title = "Resep",
                        Allow = allowEResep,
                        Child = null,
                        Target = "tab_eResep",
                        ViewLocation = "~/Views/Resep/Index.cshtml"
                    },
                    new Pasien_MenuAkses()
                    {
                        Title = "Penunjang",
                        Allow = allowEPenunjang,
                        Child = null,
                        Target = "tab_ePenunjang",
                        ViewLocation = "~/Views/OrderPenunjang/Index.cshtml"
                    },
                },
            });
            menuList.Add(new Pasien_MenuAkses()
            {
                Title = "Riwayat",
                Allow = true,
                Target = "tab_riwayat",
                ViewLocation = "",
                Child = new List<Pasien_MenuAkses>()
                {
                    new Pasien_MenuAkses()
                    {
                        Title = "Realisasi Obat",
                        Allow = true,
                        Target = "tab_obat",
                        ViewLocation = "~/Views/Obat/Index.cshtml",
                        Child = null,
                    },
                    new Pasien_MenuAkses()
                    {
                        Title = "Hasil Lab",
                        Allow = true,
                        Target = "tab_lab",
                        ViewLocation = "~/Views/HasilBacaLab/Index.cshtml",
                        Child = null,
                    },
                    new Pasien_MenuAkses()
                    {
                        Title = "Hasil Radiologi & Penunjang Lain",
                        Allow = true,
                        Target = "tab_radiologi",
                        ViewLocation = "~/Views/HasilBacaRadiologi/Index.cshtml",
                        Child = null,
                    },
                    new Pasien_MenuAkses()
                    {
                        Title = "E-Dokumen",
                        Allow = true,
                        Target = "tab_edokumen",
                        ViewLocation = "~/Views/EDokumen/Index.cshtml",
                        Child = null,
                    },
                }
            });

            return menuList;
        }
    }
}
