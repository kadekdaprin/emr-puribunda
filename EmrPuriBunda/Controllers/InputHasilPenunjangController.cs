using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.Extensions.Options;
using EmrPuriBunda.Mahas.Helpers;
using EmrPuriBunda.FileManager;
using EmrPuriBunda.Constant;

namespace EmrPuriBunda.Controllers
{
    public class InputHasilPenunjangController : BaseController
    {
        private readonly FileManagerService fileManager;
        private readonly AppSettings appSettings;

        public InputHasilPenunjangController(IConfiguration configuration, FileManagerService fileManager, IOptions<AppSettings> options) : base(configuration)
        {
            this.fileManager = fileManager;
            appSettings = options.Value;
        }

        [Authorize(Policy = PolicyConstant.InputPenunjang)]
        public async Task<IActionResult> IndexAsync()
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

            var dokter = await GetDokterAsync(userid);

            var errorView = ValidasiHarusDokter(dokter);

            if (errorView != null) return errorView;

            return View(dokter);
        }

        [HttpPost]
        public async Task<JsonResult> Datas_HasilBacaPenunjang(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<InputHasilPenunjang_HasilBacaPenunjang_ListModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "NoBukti",
                        "NRM",
                        "NamaPasien",
                        "JenisKelamin",
                        "TanggalPeriksa",
                        "SectionID_Text"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "NRM":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@NRM", $"%{x.Value}%"));
                                wheres.Add($"NRM LIKE @NRM");
                                break;
                            case "Nama":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@Nama", $"%{x.Value}%"));
                                wheres.Add($"NamaPasien LIKE @Nama");
                                break;
                            case "JenisPenunjang":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@JenisPenunjang", $"{x.Value}"));
                                wheres.Add($"Id_mJenisPenunjang = @JenisPenunjang");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_HasilBacaPenunjang
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<InputHasilPenunjang_HasilBacaPenunjang_ListModel>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpGet]
        [ActionName("HasilBacaPenunjang")]
        public async Task<JsonResult> Data_HasilBacaPenunjang(string NoBukti)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                InputHasilPenunjang_HasilBacaPenunjang_SetupModel data;
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_HasilBacaPenunjang
                        WHERE
                            NoBukti=@NoBukti
                    ";

                    data = await s.GetData<InputHasilPenunjang_HasilBacaPenunjang_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoBukti", NoBukti),
                    });

                    query = $@"						SELECT							*						FROM							trHasilBacaPenunjangDetail						WHERE							NoBukti = @NoBukti					";
                    data.tablePathDokumen = await s.GetDatas<InputHasilPenunjang_HasilBacaPenunjang_tablePathDokumen_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoBukti", NoBukti),
                    });

                    data.tablePathDokumen?.ForEach(x =>
                    {   
                        x.UrlDokumen = ConvertToUrl(x.NoBukti, x.NoUrut);
                        x.PathDokumen = "";
                    });

                }
                if (data == null) throw new Exception("Data tidak ditemukan");
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [Authorize(Policy = PolicyConstant.InputPenunjang)]
        [HttpPost]
        [ActionName("HasilBacaPenunjang")]
        public async Task<JsonResult> Post_HasilBacaPenunjang(InputHasilPenunjang_HasilBacaPenunjang_SetupModel model, List<IFormFile> DokumenHasilBaca)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

            if (DokumenHasilBaca == null || DokumenHasilBaca.Count == 0)
            {
                ModelState.AddModelError("DokumenHasilBaca", "Dokumen harus diisi");
            }

            DokumenHasilBaca.ForEach(x =>
            {
                if (x.Length > 5 * 1024 * 1024) ModelState.AddModelError("", $"Ukuran file maksimal 5mb");
                if (!x.ContentType.Equals("application/pdf", StringComparison.OrdinalIgnoreCase)) ModelState.AddModelError("", $"Ektensi file harus pdf");
            });

            if (!ModelState.IsValid)
            {
                return InvalidModelResult();
            }

            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    if (DokumenHasilBaca == null || DokumenHasilBaca.Count == 0) throw new Exception("Dokumen hasil baca harus diisi.");

                    ValidateSectionPenunjang(model.SectionID);

                    var petugasLogin = await GetDokterAsync(userid);

                    model.NoBukti = await GetNoBukti_AutoIdAsync();
                    model.PetugasID = petugasLogin.DokterID;
                    model.UserID = userid;
                    model.Tanggal = DateTime.Now;

                    model.tablePathDokumen = await fileManager.PostDokumenPenunjangAsync(model, DokumenHasilBaca);

                    s.OpenTransaction();
                    await s.Insert(model);
                    if (model.tablePathDokumen == null) model.tablePathDokumen = new List<InputHasilPenunjang_HasilBacaPenunjang_tablePathDokumen_SetupModel>();
                    foreach (var x in model.tablePathDokumen)
                    {
                        await s.Insert(x);
                    }
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }

        [Authorize(Policy = PolicyConstant.InputPenunjang)]
        [HttpPut]
        [ActionName("HasilBacaPenunjang")]
        public async Task<JsonResult> Put_HasilBacaPenunjang(string NoBukti, InputHasilPenunjang_HasilBacaPenunjang_SetupModel model, List<IFormFile> DokumenHasilBaca, List<int> noUrutEdit)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

            DokumenHasilBaca.ForEach(x =>
            {
                if (x.Length > 5 * 1024 * 1024) ModelState.AddModelError("", $"Ukuran file maksimal 5mb");
                if (!x.ContentType.Equals("application/pdf", StringComparison.OrdinalIgnoreCase)) ModelState.AddModelError("", $"Ektensi file harus pdf");
            });

            if (!ModelState.IsValid)
            {
                return InvalidModelResult();
            }

            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    ValidateSectionPenunjang(model.SectionID);

                    var oldModel = new InputHasilPenunjang_HasilBacaPenunjang_SetupModel();

                    var queryHeader = "";
                    queryHeader = $@"						SELECT							*						FROM							Vw_HasilBacaPenunjang						WHERE							NoBukti = @NoBukti					";

                    var paramNoBukti = new List<SqlParameter>
                    {
                        new SqlParameter("@NoBukti", NoBukti)
                    };

                    oldModel = await s.GetData<InputHasilPenunjang_HasilBacaPenunjang_SetupModel>(queryHeader, paramNoBukti);

                    if (oldModel == null) throw new Exception("Data tidak ditemukan");

                    oldModel.SectionID = model.SectionID;
                    oldModel.HasilBaca = model.HasilBaca;
                    oldModel.CriticalValue = model.CriticalValue;
                    oldModel.VendorRujukanID = model.VendorRujukanID;
                    oldModel.TanggalPeriksa = model.TanggalPeriksa;
                    oldModel.SectionAsalID = model.SectionAsalID;
                    oldModel.DokterID = model.DokterID;
                    oldModel.Pemeriksaan = model.Pemeriksaan;

                    var detail = await s.GetDatas<InputHasilPenunjang_HasilBacaPenunjang_tablePathDokumen_SetupModel>("SELECT * FROM trHasilBacaPenunjangDetail WHERE NoBukti = @NoBukti", new List<SqlParameter>()
                    {
                        new SqlParameter("NoBukti", NoBukti)
                    });


                    var fileToDelete = detail.Where(x => !noUrutEdit.Contains(x.NoUrut)).ToList();
                    var fileToKeep = detail.Where(x => noUrutEdit.Contains(x.NoUrut)).ToList();

                    var maxFileDelete = fileToDelete.Count > 0  ? fileToDelete.Max(x => x.NoUrut): 0;
                    var maxFileToKeep = fileToKeep.Count > 0 ? fileToKeep.Max(x => x.NoUrut) : 0;

                    int noUrutMax = maxFileDelete > maxFileToKeep ? maxFileDelete : maxFileToKeep;
                    noUrutMax = noUrutMax == 0 ? 1 : noUrutMax;

                    model.tablePathDokumen = await fileManager.PostDokumenPenunjangAsync(model, DokumenHasilBaca, fileToDelete.Select(x => x.PathDokumen).ToList(), noUrutMax);

                    s.OpenTransaction();

                    await s.Update(oldModel);

                    foreach (var item in fileToDelete)
                    {
                        await s.Delete(item);
                    }

                    if (model.tablePathDokumen == null) model.tablePathDokumen = new List<InputHasilPenunjang_HasilBacaPenunjang_tablePathDokumen_SetupModel>();

                    foreach (var x in model.tablePathDokumen)
                    {
                        await s.Insert(x);
                    }

                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }        // RoleX
        [HttpDelete]
        [ActionName("HasilBacaPenunjang")]
        public async Task<JsonResult> Delete_HasilBacaPenunjang(string NoBukti)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    var model = new InputHasilPenunjang_HasilBacaPenunjang_SetupModel();

                    var query = "";
                    query = $@"						SELECT							*						FROM							trHasilBacaPenunjangDetail						WHERE							NoBukti=@NoBukti					";
                    model.tablePathDokumen = await s.GetDatas<InputHasilPenunjang_HasilBacaPenunjang_tablePathDokumen_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoBukti", NoBukti)
                    });

                    s.OpenTransaction();
                    foreach (var x in model.tablePathDokumen) await s.Delete(x);
                    model.NoBukti = NoBukti;
                    await s.Delete(model);
                    s.Transaction.Commit();

                    foreach (var x in model.tablePathDokumen)
                    {
                        await fileManager.DeleteDokumenPenunjangAsync(x.PathDokumen);
                    }
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }


        [HttpGet]
        [ActionName("KomentarPenunjang")]
        public async Task<JsonResult> Data_KomentarPenunjang(string NoBukti)
        {
            return await Data_HasilBacaPenunjang(NoBukti);
        }

        [HttpPut]
        [ActionName("KomentarPenunjang")]
        public async Task<JsonResult> Put_HasilBacaPenunjang(string NoBukti, InputHasilPenunjang_KomentarPenunjang_SetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    string query = @"
                        IF(EXISTS(SELECT TOP 1 NoBukti FROM trHasilBacaPenunjangReview WHERE NoBukti = @NoBukti))
                        BEGIN
	
	                        UPDATE trHasilBacaPenunjangReview SET 
		                        Dibaca = @Dibaca,
		                        Komentar = @Komentar,
		                        DokterID = @DokterID
	                        WHERE 
		                        NoBukti = @NoBukti

                        END
                        ELSE
                        BEGIN

	                        INSERT INTO trHasilBacaPenunjangReview VALUES (@NoBukti, @Dibaca, @Komentar, @DokterID)

                        END
                    ";

                    s.OpenTransaction();

                    await s.ExecuteNonQuery(query, new List<SqlParameter>()
                    {
                        new SqlParameter("@NoBukti", (object) NoBukti ?? DBNull.Value),
                        new SqlParameter("@Dibaca", (object) model.Dibaca ?? DBNull.Value),
                        new SqlParameter("@Komentar", (object) model.Komentar ?? DBNull.Value),
                        new SqlParameter("@DokterID", (object) model.DokterID ?? DBNull.Value),
                    });

                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }

        [Route("InputHasilPenunjang/File/{noBukti}/{noUrut}")]
        public async Task<IActionResult> GetFileAsync(string noBukti, short noUrut)
        {
            using var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection"));

            var query = "SELECT PathDokumen FROM trHasilBacaPenunjangDetail WHERE NoBukti = @NoBukti AND NoUrut = @NoUrut";

            var data = await s.GetData<InputHasilPenunjang_HasilBacaPenunjang_tablePathDokumen_SetupModel>(query, new List<SqlParameter>()
                {
                    new SqlParameter("@NoBukti", noBukti),
                    new SqlParameter("@NoUrut", noUrut),
                });

            var result = fileManager.GetDokumenPenunjang(data.PathDokumen);

            return File(result, "application/pdf");
        }


        #region Vw_SELECT & AUTO ID
        [HttpPost]
        public async Task<JsonResult> LookUp_lookupPasien(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<Pasien_PasienRekamMedis_ListModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "NRM",
                        "NamaPasien",
                        "JenisKelamin",
                        "TglLahir",
                        "Alamat"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Filter":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@Filter", $"%{x.Value}%"));
                                wheres.Add($"(NamaPasien LIKE @Filter OR NRM LIKE @Filter )");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            mPasien
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<Pasien_PasienRekamMedis_ListModel>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPost]
        public async Task<JsonResult> LookUp_lookupBillPenunjang(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<InputHasilPenunjang_LookUpBillPenunjang> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "NoBillPenunjang",
                        "Nrm",
                        "NamaPasien",
                        "TanggalPeriksa",
                        "DokterID_Text",
                        "SectionID_Text",
                        "SectionAsalID_Text"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Filter":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@Filter", $"%{x.Value}%"));
                                wheres.Add($"(NamaPasien LIKE @Filter OR NRM LIKE @Filter )");
                                break;
                        }
                    }

                    var section = Request.GetSection();
                    if (section != null)
                    {
                        parameters.Add(new SqlParameter($"@SectionID", section.SectionID));
                        wheres.Add($"SectionID = @SectionID");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_LookUpBillPenunjang
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<InputHasilPenunjang_LookUpBillPenunjang>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);

                    var queryPemeriksaan = "SELECT dbo.Vw_LookUpBillPenunjang_Jasa(@NoBukti) AS Pemeriksaan";

                    foreach (var data in datas)
                    {
                        var periksa = await s.GetData<InputHasilPenunjang_LookUpBillPenunjang>(queryPemeriksaan, new List<SqlParameter>()
                        {
                            new SqlParameter("@NoBukti", data.NoBillPenunjang)
                        });

                        data.Pemeriksaan = periksa.Pemeriksaan;
                    }
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPost]
        public async Task<JsonResult> Vw_Select_TempatPeriksa(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Nama_Supplier LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            Kode_Supplier  AS [Value]
	                        , Nama_Supplier AS [Text]
                        FROM
                            Vw_VendorPenunjang
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);

                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPost]
        public async Task<JsonResult> Vw_Select_SectionPenunjang(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("SectionName LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            SectionID	AS [Value]
                            , SectionName AS [Text]
                        FROM
                            Vw_SectionPenunjang
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPost]
        public async Task<JsonResult> Vw_Select_Section(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("SectionName LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            SectionID	AS [Value]
                            , SectionName AS [Text]
                        FROM
                            SIMmSection
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPost]
        public async Task<JsonResult> Vw_Select_Dokter(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("NamaDOkter LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            DokterID AS [Value]
                            , NamaDOkter AS [Text]
                        FROM
                            Vw_Dokter
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpGet]
        public async Task<JsonResult> AutoId_noBukti()
        {
            try
            {
                var id = await GetNoBukti_AutoIdAsync();

                return Json(new
                {
                    Success = true,
                    Id = id
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        private async Task<string> GetNoBukti_AutoIdAsync()
        {
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                var dateCode = "yyyyMMdd";
                var baseCode = "@{DATE}EPNJ-######";
                var tempCode = MahasConverter.AutoIdGetTempCode(baseCode, dateCode);

                var parameters = new List<SqlParameter>()
                    {
                        new SqlParameter("@NoBukti", $"%{tempCode}%")
                    };
                var wheres = new List<string>()
                    {
                        "NoBukti LIKE @NoBukti"
                    };

                var query = $@"
                        SELECT
                            *
                        FROM
                            trHasilBacaPenunjang
                        {s.ToWhere(wheres)}
                        ORDER BY NoBukti DESC
                    ";

                var data = await s.GetData<InputHasilPenunjang_HasilBacaPenunjang_SetupModel>(query, parameters);
                string id = MahasConverter.AutoId(baseCode, dateCode, data?.NoBukti);

                return id;
            }
        }


        #endregion

        #region HELPER

        public void ValidateSectionPenunjang(string sectionId)
        {
            var SectionLab = appSettings.DefaultConfig.SectionLab;

            var SectionRadioLogi = appSettings.DefaultConfig.SectionRadiologi;

            if (!(SectionLab.Contains(sectionId) || SectionRadioLogi.Contains(sectionId)))
            {
                throw new Exception("Jenis penunjang harus Radiologi / Laboratorium.");
            }
        }

        private string ConvertToUrl(string noBukti, int noUrut)
        {
            return Url.Action("GetFile", "InputHasilPenunjang", new { noBukti, noUrut }, Request.Scheme, null, null);
        }

        #endregion
    }
}