using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using System.IO;
using EmrPuriBunda.Constant;
using EmrPuriBunda.Mahas.Helpers;
using System.Text;

namespace EmrPuriBunda.Controllers
{
    // RoleX
    [Authorize(Policy = PolicyConstant.SadurAsesmen)]
    public class AsesmenDokterUGDController : BasePengisianRMController
    {
        public AsesmenDokterUGDController(IConfiguration config) : base(config)
        {
        }

        [HttpGet]
        [ActionName("ActionAsesmenDokterUGD")]
        public async Task<JsonResult> Data_AsesmenDokterUGD(string NoReg)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                AsesmenDokterUGD_SetupModel data;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_AsesmenDokterUGD
                        WHERE
                            NoReg=@NoReg
                    ";

                    data = await s.GetData<AsesmenDokterUGD_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoReg", NoReg),
                    });

                    query = $@"
						SELECT
							*
						FROM
							Vw_AsesmenDokterUGD_DiagnosaSekunder
						WHERE
							NoReg=@NoReg
                        ORDER BY 
                            NoUrut
					";

                    if (data != null)
                    {
                        data.BodyDiagramView = data.BodyDiagram != null? Encoding.UTF8.GetString(data.BodyDiagram) : null;
                        data.table1 = await s.GetDatas<AsesmenDokterUGD_AsesmenDokterUGDtable1SetupModel>(query, new List<SqlParameter>
                        {
                            new SqlParameter("@NoReg", NoReg)
                        });
                    }
                }
                if (data == null) throw new Exception("Data tidak ditemukan");
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        // RoleX
        [HttpPost]
        [ActionName("ActionAsesmenDokterUGD")]
        public async Task<JsonResult> Post_AsesmenDokterUGD(AsesmenDokterUGD_SetupModel model)
        {
            if (model.SimpanSementara == false && !ModelState.IsValid)
            {
                return InvalidModelResult();
            }

            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var dokter = await GetDokterAsync();
            var section = Request.GetSection();

            try
            {
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    model.UserId = userid;
                    model.DokterId = dokter.DokterID;
                    model.SectionId = section.SectionID;
                    model.BodyDiagram = !string.IsNullOrEmpty(model.BodyDiagramView) ? Encoding.UTF8.GetBytes(model.BodyDiagramView) : null;

                    s.OpenTransaction();
                    await s.Insert(model);
                    if (model.table1 == null) model.table1 = new List<AsesmenDokterUGD_AsesmenDokterUGDtable1SetupModel>();
                    foreach (var (x, i) in model.table1.Select((y, i) => (y, i)))
                    {
                        var detailModel = new AsesmenDokterUGD_AsesmenDokterUGDtable1SetupModel()
                        {
                            NoUrut = i + 1,
                            NoReg = model.NoReg,
                            KodeICD = x.KodeICD
                        };

                        await s.Insert(detailModel);
                    }

                    if (model.SimpanSementara != true)
                    {
                        await s.ExecuteNonQuery("EXEC Sp_InsertCpptAsesmenDokterUGD @NoReg", new List<SqlParameter>()
                        {
                            new SqlParameter("@NoReg", model.NoReg)
                        });
                    }

                    s.Transaction.Commit();
                }

                if (
                    !string.IsNullOrEmpty(model.Anamnesis) &&
                    (model.table1?.Count > 0 || !string.IsNullOrEmpty(model.DiagnosaUtamaKeterangan)) &&
                    !string.IsNullOrEmpty(model.Planning)
                )
                {
                    await SetPengisianEmrAsync(model.NoReg, AsesmenDokterUGD: true);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }

        // RoleX
        [HttpPut]
        [ActionName("ActionAsesmenDokterUGD")]
        public async Task<JsonResult> Put_AsesmenDokterUGD(string NoReg, AsesmenDokterUGD_SetupModel model)
        {
            if (!ModelState.IsValid)
            {
                return InvalidModelResult();
            }

            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var dokter = await GetDokterAsync();
            var section = Request.GetSection();

            try
            {
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    model.UserId = userid;
                    model.DokterId = dokter.DokterID;
                    model.SectionId = section.SectionID;
                    model.BodyDiagram = !string.IsNullOrEmpty(model.BodyDiagramView) ? Encoding.UTF8.GetBytes(model.BodyDiagramView) : null;

                    var oldModel = new AsesmenDokterUGD_SetupModel();
                    var queryOldModel = $@"
						SELECT
							*
						FROM
							AsesmenDokterUGD
						WHERE
							NoReg=@NoReg";

                    oldModel = await s.GetData<AsesmenDokterUGD_SetupModel>(queryOldModel, new List<SqlParameter> { new SqlParameter("@NoReg", NoReg) });

                    if (oldModel.UserId.Equals(userid, StringComparison.OrdinalIgnoreCase) == false) throw new Exception("Login salah. Hanya user yang menyimpan yang boleh mengubah.");

                    var query = "";
                    query = $@"
						SELECT
							*
						FROM
							AsesmenDokterUGD_DiagnosaSekunder
						WHERE
							NoReg=@NoReg
					";
                    oldModel.table1 = await s.GetDatas<AsesmenDokterUGD_AsesmenDokterUGDtable1SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoReg", NoReg)
                    });

                    s.OpenTransaction();

                    model.NoReg = oldModel.NoReg;
                    //model.WaktuPelayanan = oldModel.WaktuPelayanan;

                    await s.Update(model);

                    foreach (var x in oldModel.table1)
                    {
                        await s.Delete(x);
                    }

                    model.table1 ??= new List<AsesmenDokterUGD_AsesmenDokterUGDtable1SetupModel>();

                    foreach (var (x, i) in model.table1.Select((y, i) => (y, i)))
                    {
                        var detailModel = new AsesmenDokterUGD_AsesmenDokterUGDtable1SetupModel()
                        {
                            NoUrut = i + 1,
                            NoReg = model.NoReg,
                            KodeICD = x.KodeICD
                        };

                        await s.Insert(detailModel);
                    }

                    if (oldModel.Id_Cppt == null)
                    {
                        await s.ExecuteNonQuery("EXEC Sp_InsertCpptAsesmenDokterUGD @NoReg", new List<SqlParameter>()
                        {
                            new SqlParameter("@NoReg", model.NoReg)
                        });
                    }
                    else
                    {
                        await s.ExecuteNonQuery("EXEC Sp_UpdateCpptAsesmenDokterUGD @NoReg", new List<SqlParameter>()
                        {
                            new SqlParameter("@NoReg", model.NoReg)
                        });
                    }

                    s.Transaction.Commit();
                }

                if (
                    !string.IsNullOrEmpty(model.Anamnesis) &&
                    (model.table1?.Count > 0 || !string.IsNullOrEmpty(model.DiagnosaUtamaKeterangan)) &&
                    !string.IsNullOrEmpty(model.Planning)
                )
                {
                    await SetPengisianEmrAsync(model.NoReg, AsesmenDokterUGD: true);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }

            return Json(new
            {
                Success = true,
                Data = new
                {
                    NoReg = NoReg
                }
            });
        }

        // RoleX
        [HttpDelete]
        [ActionName("ActionAsesmenDokterUGD")]
        public async Task<JsonResult> Delete_AsesmenDokterUGD(string NoReg)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    var model = new AsesmenDokterUGD_SetupModel();
                    var query = "";
                    query = $@"
						SELECT
							*
						FROM
							AsesmenDokterUGD_DiagnosaSekunder
						WHERE
							NoReg=@NoReg
					";
                    model.table1 = await s.GetDatas<AsesmenDokterUGD_AsesmenDokterUGDtable1SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoReg", NoReg)
                    });

                    s.OpenTransaction();
                    foreach (var x in model.table1) await s.Delete(x);
                    model.NoReg = NoReg;
                    await s.Delete(model);
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }

        [HttpPost]
        public async Task<JsonResult> Vw_Select_JenisKunjungan(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Select_JenisKunjungan
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> Vw_Select_Triage(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Select_Triage
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> Vw_Select_Rujukan(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Select_Rujukan
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> Vw_Select_KeadaanSaatDatang(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Select_KeadaanSaatDatang
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> Vw_Select_AlasanKunjungan(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Select_AlasanKunjungan
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> Vw_Select_CaraDatang(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Select_CaraDatang
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> Vw_Select_HubunganPasien(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Select_HubunganPasien
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> LookUp_lookupDiagnosa(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<AsesmenDokterUGD_lookupDiagnosaLookUpModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "KodeICD",
                        "Descriptions"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Filter":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@Filter", $"%{x.Value}%"));
                                wheres.Add($"(KodeICD LIKE @Filter OR Descriptions LIKE @Filter)");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_ICD
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<AsesmenDokterUGD_lookupDiagnosaLookUpModel>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> LookUp_table1(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<AsesmenDokterUGD_tableLookUpModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "KodeICD",
                        "Descriptions"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Filter":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@Filter", $"%{x.Value}%"));
                                wheres.Add($"(KodeICD LIKE @Filter OR Descriptions LIKE @Filter)");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_ICD
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<AsesmenDokterUGD_tableLookUpModel>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }
    }
}