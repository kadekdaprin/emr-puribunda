﻿using EmrPuriBunda.Models;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Constant;
using Microsoft.AspNetCore.Authorization;

namespace EmrPuriBunda.Controllers
{
    [Authorize(Policy = PolicyConstant.InputEDokumen)]
    public class PasienRekamMedisController : BaseController
    {
        public PasienRekamMedisController(IConfiguration config) : base(config)
        {
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Datas_PasienRekamMedis(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<Pasien_PasienRekamMedis_ListModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "NRM",
                        "NamaPasien",
                        "JenisKelamin",
                        "TglLahir",
                        "Alamat"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Filter":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@Filter", $"%{x.Value}%"));
                                wheres.Add($"(NamaPasien LIKE @Filter OR NRM LIKE @Filter )");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            mPasien
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<Pasien_PasienRekamMedis_ListModel>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }
    }
}
            

