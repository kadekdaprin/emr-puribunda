using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using EmrPuriBunda.Constant;

namespace EmrPuriBunda.Controllers
{
    [Authorize]
    public class RegisterDpjpController : BaseController
    {
        public RegisterDpjpController(IConfiguration config) : base(config)
        {
        }
        [HttpGet]
        [ActionName("RegisterDpjp")]
        public async Task<JsonResult> Data_RegisterDpjp(string NoReg, int Nomor)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                RegisterDpjp_SetupModel data;

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var query = @"
                        SELECT 
	                        DokterId = SIMtrDataRegPasien.DokterID,
	                        DokterId_Text = mSupplier.Nama_Supplier
                        FROM SIMtrDataRegPasien
	                        LEFT JOIN mSupplier ON mSupplier.Kode_Supplier = SIMtrDataRegPasien.DokterID
                        WHERE 
                            NoReg = @NoReg
                            AND Nomor = @Nomor
                    ";

                    data = await s.GetData<RegisterDpjp_SetupModel>(query, new List<SqlParameter>()
                    {
                        new SqlParameter("@NoReg", NoReg),
                        new SqlParameter("@Nomor", Nomor),
                    });

                    data ??= new RegisterDpjp_SetupModel();
                }

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var queryDetail = $@"						SELECT							*						FROM							Vw_trRegisterDokterDPJPStatus						WHERE							NoReg = @NoReg					";					data.tabelRegisterDpjp = await s.GetDatas<RegisterDpjp_RegisterDpjp_tabelRegisterDpjp_SetupModel>(queryDetail, new List<SqlParameter>					{                        new SqlParameter("@NoReg", NoReg),

                    });
                }
                if (data == null) throw new Exception("Data tidak ditemukan");
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        // RoleX
        [HttpPut]
        [ActionName("RegisterDpjp")]
        public async Task<JsonResult> Put_RegisterDpjp(string NoReg, int Nomor, RegisterDpjp_SetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using(var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
            {
                try
                {
                    s.OpenTransaction();

                    await s.ExecuteNonQuery("EXEC EMR_UpdateDPJP @NoReg, @Nomor, @DokterID ", new List<SqlParameter>()
                    {
                        new SqlParameter("@NoReg", NoReg),
                        new SqlParameter("@Nomor", Nomor),
                        new SqlParameter("@DokterID", model.DokterId),
                    });

                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    model.tabelRegisterDpjp?.ForEach(x => x.NoReg = NoReg);

                    var oldModel = new RegisterDpjp_SetupModel();					var query = "";					query = $@"						SELECT							*						FROM							trRegisterDokterDPJPStatus						WHERE							NoReg = @NoReg					";					oldModel.tabelRegisterDpjp = await s.GetDatas<RegisterDpjp_RegisterDpjp_tabelRegisterDpjp_SetupModel>(query, new List<SqlParameter>					{						new SqlParameter("@NoReg", NoReg)					});

                    model.tabelRegisterDpjp ??= new List<RegisterDpjp_RegisterDpjp_tabelRegisterDpjp_SetupModel>();

                    s.OpenTransaction();					foreach (var x in oldModel.tabelRegisterDpjp)					{						if (model.tabelRegisterDpjp.FirstOrDefault(y =>							y.NoReg == x.NoReg &&							y.DokterID == x.DokterID &&							y.Id_mStatusDPJP == x.Id_mStatusDPJP						) == null)						{							await s.Delete(x);						}					}					foreach (var x in model.tabelRegisterDpjp)					{												if (oldModel.tabelRegisterDpjp.FirstOrDefault(y =>							y.NoReg == x.NoReg &&							y.DokterID == x.DokterID &&							y.Id_mStatusDPJP == x.Id_mStatusDPJP						) == null)						{							await s.Insert(x);						}					}
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }        [HttpPost]
        public async Task<JsonResult> Select_StatusDpjp(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Status LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            Id AS Value,
                            Status AS Text
                        FROM
                            mStatusDPJP
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPost]
        public async Task<JsonResult> Vw_Select_Dokter(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Select_Dokter
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPost]
        public async Task<JsonResult> LookUp_tabelRegisterDpjp(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<RegisterDpjp_tabelRegisterDpjp_LookUpModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "DokterID",						"NamaDOkter",						"Alamat",						"NoKontak"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Nama":								if (string.IsNullOrEmpty(x.Value)) break;								parameters.Add(new SqlParameter($"@Nama", $"%{x.Value}%"));								wheres.Add($"NamaDOkter LIKE @Nama");								break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            mDokter
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<RegisterDpjp_tabelRegisterDpjp_LookUpModel>(query, _orderBy[orderBy], orderByType,  pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }
    }
}