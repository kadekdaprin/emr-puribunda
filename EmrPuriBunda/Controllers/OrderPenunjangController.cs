using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using EmrPuriBunda.Constant;
using EmrPuriBunda.Mahas.Helpers;

namespace EmrPuriBunda.Controllers
{
    [Authorize(Policy = PolicyConstant.EPenunjang)]
    public class OrderPenunjangController : BaseController
    {
        public OrderPenunjangController(IConfiguration config) : base(config)
        {
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Datas_OrderPenunjang(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<OrderPenunjang_OrderPenunjang_ListModel> datas;
                
                var section = Request.GetSection();
                var dokter = await GetDokterAsync();

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var wheres = new List<string>()
                    {
                        "SectionAsalID = @SectionAsalID"
                    };

                    var parameters = new List<SqlParameter>()
                    {
                        new SqlParameter("@SectionAsalID", section.SectionID)
                    };

                    if (User.IsRoleAdmin() == false && User.IsRolePerawatBidan() == false)
                    {
                        wheres.Add("(DokterID = @DokterID OR UserId = @UserId)");
                        parameters.Add(new SqlParameter("@DokterID", (object)dokter?.DokterID ?? DBNull.Value));
                        parameters.Add(new SqlParameter("@UserId", userid));
                    }

                    var _orderBy = new List<string>()
                    {
                        "Tanggal",
                        "NoBukti",						"NamaVendor",
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "NoReg":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@NoReg", x.Value));
                                wheres.Add($"(NoReg LIKE @NoReg)");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
							,'OrderPenunjang'= 
							    coalesce(dbo.trPenunjang_detailtindakan_manualgabungan(NoBukti), '')
							    + ', ' + coalesce(dbo.trPenunjang_detailtindakangabungan(NoBukti), '')
                            ,'CatatanPemeriksaan'= coalesce(PemeriksaanTambahan,'')
                        FROM 
                            EMR_OrderPenunjang
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<OrderPenunjang_OrderPenunjang_ListModel>(query, _orderBy[orderBy], orderByType,  pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpGet]
        [ActionName("OrderPenunjang")]
        public async Task<JsonResult> Data_OrderPenunjang(string NoBukti)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                OrderPenunjang_OrderPenunjang_SetupModel data;
                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var query = $@"
                        SELECT 
	                        trOrderPenunjang .*,
	                        SIMmSection.SectionName
                        FROM trOrderPenunjang
	                        LEFT JOIN SIMmSection ON SIMmSection.SectionID = trOrderPenunjang.SectionID
                        WHERE
                            NoBukti=@NoBukti
                    ";

                    data = await s.GetData<OrderPenunjang_OrderPenunjang_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoBukti", NoBukti),
                    });

                    if (data == null) throw new Exception("Data tidak ditemukan");

                    var queryDetail = $@"
                        select 
	                        JasaID AS JasaID, 
	                        ID_Kelompok AS KelompokID
                        from trOrderPenunjangDetail 
                        where NoBukti = @NoBukti
                    ";

                    data.JasaList = await s.GetDatas<JasaKelompok>(queryDetail, new List<SqlParameter>() {
                        new SqlParameter("@NoBukti", NoBukti)
                    });

                    var queryDetail2 = $@"
                        SELECT							*						FROM							Vw_OrderPenunjangDetailManual						WHERE
                            NoBukti = @NoBukti
                    ";

                    data.tb_orderPenunjangDetailManual = await s.GetDatas<OrderPenunjang_DetailManual_SetupModel>(queryDetail2, new List<SqlParameter>() {
                        new SqlParameter("@NoBukti", NoBukti)
                    });

                }
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        // RoleX
        [HttpPost]
        [ActionName("OrderPenunjang")]
        public async Task<JsonResult> Post_OrderPenunjang(OrderPenunjang_OrderPenunjang_SetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

            if (!ModelState.IsValid)
            {
                return InvalidModelResult();
            }

            using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
            {
                try
                {
                    model.UserId = userid;
                    model.Tanggal = DateTime.Now;
                    model.SectionAsalID = Request.GetSection()?.SectionID;

                    s.OpenTransaction();

                    model.JasaList ??= new List<JasaKelompok>();
                    model.tb_orderPenunjangDetailManual ??= new List<OrderPenunjang_DetailManual_SetupModel>();

                    if ((string.IsNullOrEmpty(model.PemeriksaanTambahan) &&
                        model.JasaList.Count == 0 &&
                        model.tb_orderPenunjangDetailManual.Count == 0) == true) throw new Exception("Tindakan harus diisi");

                    model.NoBukti = await s.ExecuteScalar<string>(
                    @"
                        EXEC trOrder_insert_New
                        @Tanggal
                        , @Noreg
                        , @NamaPasien
                        , @Hasil_Cito
                        , @Hasil_Rutin
                        , @KodeICD
                        , @DiagnosaIndikasi
                        , @DokterID
                        , @NamaVendor
                        , @AlamatVendor
                        , @TlpVendor
                        , @DiterimaTgl
                        , @JamSampling
                        , @JamBilling
                        , @KodePetugas
                        , @NamaPetugas
                        , @PemeriksaanTambahan
                        , @Realisasi
                        , @UserId
                        , @SectionID
                        , @SectionAsalID
                    ", new List<SqlParameter>()
                    {
                        new SqlParameter("@Tanggal", model.Tanggal),
                        new SqlParameter("@Noreg", model.Noreg),
                        new SqlParameter("@NamaPasien", model.NamaPasien),
                        new SqlParameter("@Hasil_Cito", model.Hasil_Cito),
                        new SqlParameter("@Hasil_Rutin", model.Hasil_Rutin),
                        new SqlParameter("@KodeICD", model.KodeICD),
                        new SqlParameter("@DiagnosaIndikasi", model.DiagnosaIndikasi),
                        new SqlParameter("@DokterID", model.DokterID),
                        new SqlParameter("@NamaVendor", model.NamaVendor),
                        new SqlParameter("@AlamatVendor", model.AlamatVendor),
                        new SqlParameter("@TlpVendor", model.TlpVendor),
                        new SqlParameter("@DiterimaTgl", model.DiterimaTgl),
                        new SqlParameter("@JamSampling", model.JamSampling),
                        new SqlParameter("@JamBilling", model.JamBilling),
                        new SqlParameter("@KodePetugas", model.KodePetugas),
                        new SqlParameter("@NamaPetugas", model.NamaPetugas),
                        new SqlParameter("@PemeriksaanTambahan", model.PemeriksaanTambahan),
                        new SqlParameter("@Realisasi", model.Realisasi),
                        new SqlParameter("@UserId", model.UserId),
                        new SqlParameter("@SectionID", model.SectionID),
                        new SqlParameter("@SectionAsalID", model.SectionAsalID)
                    });

                    foreach (var jasa in model.JasaList)
                    {
                        await s.ExecuteNonQuery("exec trOrderPenunjangDetail_insert @NoBukti, @ID_Kelompok, @JasaID", new List<SqlParameter>
                        {
                            new SqlParameter("@NoBukti", model.NoBukti ),
                            new SqlParameter("@JasaID", jasa.JasaID),
                            new SqlParameter("@ID_Kelompok", jasa.KelompokID )
                        }); ;
                    }

                    foreach (var (jasa, index) in model.tb_orderPenunjangDetailManual.Select((x, i) => (x, i)))
                    {
                        jasa.NoBukti = model.NoBukti;
                        jasa.NoUrut = (index + 1);

                        await s.Insert(jasa);
                    }

                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    s.Transaction.Rollback();
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }        // RoleX
        [HttpPut]
        [ActionName("OrderPenunjang")]
        public async Task<JsonResult> Put_OrderPenunjang(string NoBukti, OrderPenunjang_OrderPenunjang_SetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

            OrderPenunjang_OrderPenunjang_SetupModel oldModel;

            using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
            {
                try
                {
                    model.JasaList ??= new List<JasaKelompok>();
                    model.tb_orderPenunjangDetailManual ??= new List<OrderPenunjang_DetailManual_SetupModel>();

                    if (string.IsNullOrEmpty(model.PemeriksaanTambahan) &&
                        model.JasaList.Count == 0 &&
                        model.tb_orderPenunjangDetailManual.Count == 0) throw new Exception("Tindakan harus diisi");

                    var query = $@"
                        SELECT
                            *
                        FROM
                            trOrderPenunjang
                        WHERE
                            NoBukti=@NoBukti
                    ";

                    oldModel = await s.GetData<OrderPenunjang_OrderPenunjang_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoBukti", NoBukti),
                    });


                    if (oldModel == null) throw new Exception("Data tidak ditemukan");
                    if (oldModel.Realisasi == true) throw new Exception("Tidak dapat diubah. Data sudah direalisasi.");

                    oldModel.UserId = userid;

                    var queryDetail = $@"
                        select 
	                        JasaID AS JasaID, 
	                        ID_Kelompok AS KelompokID
                        from trOrderPenunjangDetail 
                        where NoBukti = @NoBukti
                    ";

                    s.OpenTransaction();

                    oldModel.KodeICD = model.KodeICD;
                    oldModel.DiagnosaIndikasi = model.DiagnosaIndikasi;
                    oldModel.DokterID = model.DokterID;
                    oldModel.NamaVendor = model.NamaVendor;
                    oldModel.AlamatVendor = model.AlamatVendor;
                    oldModel.TlpVendor = model.TlpVendor;
                    oldModel.PemeriksaanTambahan = model.PemeriksaanTambahan;
                    oldModel.Hasil_Cito = model.Hasil_Cito;

                    await s.Update(oldModel);

                    await s.ExecuteNonQuery("DELETE FROM trOrderPenunjangDetail WHERE NoBukti = @NoBukti", new List<SqlParameter>() {
                        new SqlParameter("@NoBukti", NoBukti)
                    });

                    model.JasaList ??= new List<JasaKelompok>();

                    foreach (var jasa in model.JasaList)
                    {
                        await s.ExecuteNonQuery("exec trOrderPenunjangDetail_insert @NoBukti, @ID_Kelompok, @JasaID", new List<SqlParameter>
                        {
                            new SqlParameter("@NoBukti", oldModel.NoBukti ),
                            new SqlParameter("@JasaID", jasa.JasaID),
                            new SqlParameter("@ID_Kelompok", jasa.KelompokID )
                        }); ;
                    }

                    await s.ExecuteNonQuery("DELETE FROM trOrderPenunjangDetailManual WHERE NoBukti = @NoBukti", new List<SqlParameter>() {
                        new SqlParameter("@NoBukti", NoBukti)
                    });

                    model.JasaList ??= new List<JasaKelompok>();

                    foreach (var (jasa, index) in model.tb_orderPenunjangDetailManual.Select((x, i) => (x,i)))
                    {
                        jasa.NoBukti = model.NoBukti;
                        jasa.NoUrut = (index + 1);

                        await s.Insert(jasa);
                    }

                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }        // RoleX
        [HttpDelete]
        [ActionName("OrderPenunjang")]
        public async Task<JsonResult> Delete_OrderPenunjang(string NoBukti)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
            {
                try
                {
                    s.OpenTransaction();

                    await s.ExecuteNonQuery("UPDATE trOrderPenunjang SET Batal = 1 WHERE NoBukti = @NoBukti", new List<SqlParameter>() {
                        new SqlParameter("@NoBukti", NoBukti)
                    });

                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }        [HttpPost]
        public async Task<JsonResult> EMR_Select_Dokter(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            DokterID AS [Value]
                            NamaDOkter AS [Text], 
                        FROM
                            Vw_Dokter
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPost]
        public async Task<JsonResult> Select_Section(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>
                    {
                        "TipePelayanan = 'PENUNJANG'"
                    };

                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            SectionID as [Value],
                            SectionName As [Text]
                        FROM
                            SIMmSection
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPost]
        public async Task<JsonResult> LookUp_tb_orderPenunjangDetailManual(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<OrderPenunjang_DetailManual_LookUpModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "JasaID",
                        "JasaName",
                        "Alias",
                        "KeteranganTindakan"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Filter":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@Filter", $"%{x.Value}%"));
                                wheres.Add($"(JasaName LIKE @Filter OR Alias LIKE @Filter)");
                                break;
                            case "SectionID":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@SectionID", x.Value));
                                wheres.Add($"(SectionID = @SectionID)");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            VW_mOrderPenunjang_ListJasaManual
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<OrderPenunjang_DetailManual_LookUpModel>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPost]
        public async Task<JsonResult> LookUp_lookup1(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<OrderPenunjang_OrderPenunjang_lookup1_LookUpModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "KodeICD",						"Descriptions"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Filter":								if (string.IsNullOrEmpty(x.Value)) break;								parameters.Add(new SqlParameter($"@Filter", $"%{x.Value}%"));								wheres.Add($"(KodeICD LIKE @Filter OR Descriptions LIKE @Filter)");								break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            mICD
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<OrderPenunjang_OrderPenunjang_lookup1_LookUpModel>(query, _orderBy[orderBy], orderByType,  pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        //DOKTER        [HttpPost]
        public async Task<JsonResult> LookUp_lookup2(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<OrderPenunjang_OrderPenunjang_lookup2_LookUpModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "DokterID",						"NamaDOkter",						"Alamat",						"NoKontak",						"SpesialisName"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Nama":								if (string.IsNullOrEmpty(x.Value)) break;								parameters.Add(new SqlParameter($"@Nama", $"%{x.Value}%"));								wheres.Add($"NamaDokter LIKE @Nama");								break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Dokter
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<OrderPenunjang_OrderPenunjang_lookup2_LookUpModel>(query, _orderBy[orderBy], orderByType,  pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpGet]
        public async Task<JsonResult> AutoId_autoid2()
        {
            try
            {
                var id = await GetNoBukti_AutoIdAsync();

                return Json(new
                {
                    Success = true,
                    Id = id
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        private async Task<string> GetNoBukti_AutoIdAsync()
        {
            using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
            {
                var dateCode = "yyyyMMdd";
                var baseCode = "@{DATE}OP-######";
                var tempCode = MahasConverter.AutoIdGetTempCode(baseCode, dateCode);

                var parameters = new List<SqlParameter>()
                    {
                        new SqlParameter("@NoBukti", $"%{tempCode}%")
                    };
                var wheres = new List<string>()
                    {
                        "NoBukti LIKE @NoBukti"
                    };

                var query = $@"
                        SELECT
                            *
                        FROM
                            trOrderPenunjang
                        {s.ToWhere(wheres)}
                        ORDER BY NoBukti DESC
                    ";

                var data = await s.GetData<OrderPenunjang_OrderPenunjang_SetupModel>(query, parameters);
                string id = MahasConverter.AutoId(baseCode, dateCode, data?.NoBukti);

                return id;
            }
        }
    }
}