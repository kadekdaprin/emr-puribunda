using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using EmrPuriBunda.Mahas.Helpers;
using EmrPuriBunda.Constant;

namespace EmrPuriBunda.Controllers
{
    [Authorize(Policy = PolicyConstant.Cppt)]
    public class CatatanKeperawatanController : BaseController
    {
        public CatatanKeperawatanController(IConfiguration config) : base(config)
        {
        }

        [HttpGet]
        [ActionName("ActionCatatanKeperawatan")]
        public async Task<JsonResult> Data_CatatanKeperawatan(int Id)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                CatatanKeperawatan_SetupModel data;
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_CatatanKeperawatan
                        WHERE
                            Id = @Id
                    ";

                    data = await s.GetData<CatatanKeperawatan_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@Id", Id),
                    });


                }
                if (data == null) throw new Exception("Data tidak ditemukan");
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> Datas_CatatanKeperawatan(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<CatatanKeperawatan_ListModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "Tanggal",						"ProfesiRole",						"Id_mJenisCatatanPerawat",						"Tindakan"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "DariTanggal":								if (string.IsNullOrEmpty(x.Value)) break;								parameters.Add(new SqlParameter($"@DariTanggal", $"{x.Value}"));								wheres.Add($"CAST(Tanggal AS DATE) >= @DariTanggal");								break;							case "SampaiTanggal":								if (string.IsNullOrEmpty(x.Value)) break;								parameters.Add(new SqlParameter($"@SampaiTanggal", $"{x.Value}"));								wheres.Add($"CAST(Tanggal AS DATE) <= @SampaiTanggal");								break;
                            case "Profesi":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@Profesi", $"%{x.Value}%"));
                                wheres.Add($"ProfesiRole LIKE @Profesi");
                                break;
                            case "JenisForm":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@JenisForm", $"{x.Value}"));
                                wheres.Add($"Id_mJenisCatatanPerawat = @JenisForm");
                                break;
                            case "NRM":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@NRM", $"{x.Value}"));
                                wheres.Add($"NRM = @NRM");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_CatatanKeperawatan
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<CatatanKeperawatan_ListModel>(query, _orderBy[orderBy], orderByType,  pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPost]
        public async Task<JsonResult> History_CatatanKeperawatan(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<CatatanKeperawatan_ListModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "Tanggal",
                        "IdHistory",
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Id":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@Id", x.Value));
                                wheres.Add($"IdCatatanKeperawatan = @Id");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_CatatanKeperawatan_History
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<CatatanKeperawatan_ListModel>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPost]
        [ActionName("ActionCatatanKeperawatan")]
        public async Task<JsonResult> Post_CatatanKeperawatan(CatatanKeperawatan_SetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    model.SectionId = Request.GetSection()?.SectionID;
                    model.UserId = userid;
                    model.LastUpdate = DateTime.Now;

                    s.OpenTransaction();

                    await s.Insert(model);

                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }

        // RoleX
        [HttpPut]
        [ActionName("ActionCatatanKeperawatan")]
        public async Task<JsonResult> Put_CatatanKeperawatan(int Id, CatatanKeperawatan_SetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

            CatatanKeperawatan_SetupModel oldModel;
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_CatatanKeperawatan
                        WHERE
                            Id = @Id
                    ";

                    oldModel = await s.GetData<CatatanKeperawatan_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@Id", Id)
                    });
                    if (oldModel == null) throw new Exception("Data tidak ditemukan");

                    s.OpenTransaction();

                    await s.ExecuteNonQuery("exec Sp_InsertCatatanKeperawatanHistory @IdCatatanKeperawatan", new List<SqlParameter>()
                    {
                        new SqlParameter("@IdCatatanKeperawatan", oldModel.Id)
                    });

                    model.Id = oldModel.Id;
                    model.Tanggal = oldModel.Tanggal;
                    model.NoReg = oldModel.NoReg;
                    model.SectionId = oldModel.SectionId;
                    model.UserId = userid;
                    model.AdaPerubahan = true;
                    model.LastUpdate = DateTime.Now;

                    await s.Update(model);

                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }

        [HttpPut]
        [ActionName("SerahTerimaCatatanKeperawatan")]
        public async Task<JsonResult> SerahTerimaCatatanKeperawatan(int id)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

                CatatanKeperawatan_SetupModel data;
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            CatatanKeperawatan
                        WHERE
                            Id = @Id
                    ";

                    data = await s.GetData<CatatanKeperawatan_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@Id", id),
                    });

                    if (data == null) throw new Exception("Data tidak ditemukan");

                    if(data.SerahTerima_UserId == userid) throw new Exception("Serah terima tidak bisa dilakukan oleh user yang melakukan penginputan.");

                    data.SudahSerahTerima = true;
                    data.SerahTerima_UserId = userid;
                    data.WaktuSerahTerima = DateTime.Now;

                    s.OpenTransaction();
                    await s.Update(data);
                    s.CommitTransaction();

                }
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPut]
        [ActionName("DoubleCheckCatatanKeperawatan")]
        public async Task<JsonResult> DoubleCheckCatatanKeperawatan(int id)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

                CatatanKeperawatan_SetupModel data;
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            CatatanKeperawatan
                        WHERE
                            Id = @Id
                    ";

                    data = await s.GetData<CatatanKeperawatan_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@Id", id),
                    });

                    if (data == null) throw new Exception("Data tidak ditemukan");

                    if (data.DoubleCheck_UserId == userid) throw new Exception("Double check tidak bisa dilakukan oleh user yang melakukan penginputan.");

                    data.SudahDoubleCheck = true;
                    data.DoubleCheck_UserId = userid;
                    data.WaktuDoubleCheck = DateTime.Now;

                    s.OpenTransaction();
                    await s.Update(data);
                    s.CommitTransaction();

                }
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPost]
        public async Task<JsonResult> mJenisCatatanKeperawatan(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Jenis LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            Id AS Value,
                            Jenis AS Text
                        FROM
                            mJenisCatatanKeperawatan
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }
    }
}