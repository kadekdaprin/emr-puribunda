﻿using EmrPuriBunda.Models;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EmrPuriBunda.Controllers
{
    public class BaseController : Controller
    {
        protected readonly IConfiguration Config;

        public BaseController(IConfiguration config)
        {
            Config = config;
        }

        protected string UserID { get => User.FindFirstValue(ClaimTypes.NameIdentifier); }

        protected ViewResult ValidasiHarusDokter(DokterModel dokter)
        {
            if (dokter == null)
            {
                return View("CustomError", new ErrorViewModel()
                {
                    Errors = null,
                    Title = "Tidak ada akses",
                    SubTitle = "Anda tidak memiliki akses menuju ke halaman ini (Harus terdaftar sebagai dokter/perawat)"
                });
            }

            return null;
        }

        protected async Task<DokterModel> GetDokterAsync(string userId = null)
        {
            userId = UserID;

            DokterModel data;

            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                var parameters = new List<SqlParameter>();
                var wheres = new List<string>();

                var query = $@"
                        SELECT 
	                        mDokter.ID
	                        , mDokter.DokterID
	                        , mDokter.NamaDOkter
	                        , mDokter.Alamat
	                        , mDokter.NoKontak
	                        , mDokter.Active
                        FROM mDokter 
	                        LEFT JOIN mUserDokter ON mDokter.DokterID = mUserDokter.DokterId
                        WHERE 
	                        mUserDokter.UserId = @Id
                    ";

                data = await s.GetData<DokterModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@Id", userId),
                    });

            }

            return data;
        }

        protected JsonResult InvalidModelResult()
        {
            var errorList = ModelState.Values
                .SelectMany(x => x.Errors)
                .Select(x => x.ErrorMessage).ToList();

            var message = string.Join("<br/>", errorList);

            return Json(new
            {
                Success = false,
                Message = message
            });
        }
    }
}
