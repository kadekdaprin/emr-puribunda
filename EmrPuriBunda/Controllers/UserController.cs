using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using EmrPuriBunda.Areas.Identity.Data;
using Microsoft.AspNetCore.Identity;
using EmrPuriBunda.Constant;
using EmrPuriBunda.Mahas.Helpers;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace EmrPuriBunda.Controllers
{
    [Authorize(Roles = RolesConstant.Admin)]
    public class UserController : BaseController
    {
        private readonly UserManager<ApplicationUser> _userManager;
        public UserController(IConfiguration configuration, UserManager<ApplicationUser> userManager) : base(configuration)
        {
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Datas_User(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<User_UserSetupModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EMRPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "UserName",
                        "Profesi",
                        "Email"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Filter":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@Filter", $"%{x.Value}%"));
                                wheres.Add($"(Username LIKE @Filter OR Email LIKE @Filter OR Profesi LIKE @Filter)");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_User
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<User_UserSetupModel>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpGet]
        [ActionName("ActionUser")]
        public async Task<JsonResult> Data_User(string Id)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                User_UserSetupModel data;
                using (var s = new MahasConnection(Config.GetConnectionString("EMRPuriBundaConnection")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_User
                        WHERE
                            Id=@Id
                    ";

                    data = await s.GetData<User_UserSetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@Id", Id),
                    });
                }
                using (var s = new MahasConnection(Config.GetConnectionString("UserIdentity")))
                {
                    var query = $@"
						SELECT
							*
						FROM
							VW_AspNetUserRoles
						WHERE
							UserId=@Id
					";
                    data.roles = await s.GetDatas<User_UserRoles>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@Id", Id),
                    });
                }
                if (data == null) throw new Exception("Data tidak ditemukan");
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        // RoleX
        [HttpPost]
        [ActionName("ActionUser")]
        public async Task<JsonResult> Post_User(User_UserSetupModel model)
        {
            try
            {
                if (model.Password != model.UlangiPassword) throw new Exception("Password dan Ulangi Password harus sama");

                ApplicationUser user = new ApplicationUser
                {
                    UserName = model.UserName,
                    Email = model.Email,
                };
                var result = await _userManager.CreateAsync(user, model.Password);

                if (!result.Succeeded)
                {
                    var errors = new List<string>();
                    foreach (var error in result.Errors)
                    {
                        errors.Add(error.Description);
                    }
                    throw new Exception(string.Join(", ", errors));
                }

                using(var s = new MahasConnection(Config.GetConnectionString("UserIdentity")))
                {
                    try
                    {
                        s.OpenTransaction();
                        foreach (var role in model.roles)
                        {
                            var userRole = new User_UserRoles()
                            {
                                RoleId = role.RoleId,
                                UserId = user.Id
                            };

                            await s.Insert(userRole);
                        }

                        s.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        return Json(new
                        {
                            Success = false,
                            ex.Message
                        });
                    }
                }

                using (var s = new MahasConnection(Config.GetConnectionString("EMRPuriBundaConnection")))
                {
                    try
                    {
                        var userDokter = new UserDokter_UserDokter_SetupModel()
                        {
                            DokterId = model.IdProfesi,
                            UserId = user.Id
                        };

                        s.OpenTransaction();
                        await s.Insert(userDokter);

                        s.Transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        return Json(new
                        {
                            Success = false,
                            ex.Message
                        });
                    }
                }

                return Json(new
                {
                    Success = true,
                    Data = model
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }

        }

        [HttpPut]
        [ActionName("ActionUser")]
        public async Task<JsonResult> Put_User(string Id, User_UserSetupModel_Update model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("UserIdentity")))
            {
                try
                {
                    var oldModel = new User_UserSetupModel();
                    var query = "";
                    query = $@"
						SELECT
							*
						FROM
							AspNetUserRoles
						WHERE
							UserId=@Id
					";
                    oldModel.roles = await s.GetDatas<User_UserRoles>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@Id", Id),
                    });

                    model.roles ??= new List<User_UserRoles>();

                    s.OpenTransaction();
                    model.Id = Id;
                    model.NormalizedUserName = model.UserName;
                    await s.Update(model);

                    foreach (var x in model.roles)
                    {
                        x.UserId = Id;
                    }

                    foreach (var x in oldModel.roles)
                    {
                        if (model.roles.FirstOrDefault(y =>
                            y.UserId == x.UserId &&
                            y.RoleId == x.RoleId
                        ) == null)
                        {
                            await s.Delete(x);
                        }
                    }
                    foreach (var x in model.roles)
                    {

                        if (oldModel.roles.FirstOrDefault(y =>
                            y.UserId == x.UserId &&
                            y.RoleId == x.RoleId
                        ) == null)
                        {
                            await s.Insert(x);
                        }
                    }

                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            using (var s = new MahasConnection(Config.GetConnectionString("EMRPuriBundaConnection")))
            {
                try
                {
                    s.OpenTransaction();

                    await s.ExecuteNonQuery("DELETE FROM mUserDokter WHERE UserId = @UserId", new List<SqlParameter>()
                    {
                        new SqlParameter("@UserId", Id),
                    });

                    await s.ExecuteNonQuery("INSERT INTO mUserDokter VALUES (@UserId, @DokterId)", new List<SqlParameter>()
                    {
                        new SqlParameter("@UserId", Id),
                        new SqlParameter("@DokterId", model.IdProfesi)
                    });

                    s.CommitTransaction();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }

        [HttpPost]
        [ActionName("ResetPassword")]
        public async Task<JsonResult> ResetPassword(User_PasswordReset model)
        {
            try
            {   
                string newPassword = "01";

                var user = await _userManager.FindByIdAsync(model.UserId);

                var token = await _userManager.GeneratePasswordResetTokenAsync(user);

                await _userManager.ResetPasswordAsync(user, token, newPassword);

                return Json(new
                {
                    Success = true
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }

        }

        // RoleX
        [HttpDelete]
        [ActionName("ActionUser")]
        public async Task<JsonResult> Delete_User(string Id)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("UserIdentity")))
            {
                try
                {
                    var model = new User_UserSetupModel();
                    var query = "";
                    query = $@"
						SELECT
							*
						FROM
							AspNetUserRoles
						WHERE
							UserId=@Id
					";
                    model.roles = await s.GetDatas<User_UserRoles>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@Id", Id),
                    });

                    s.OpenTransaction();
                    foreach (var x in model.roles) await s.Delete(x);
                    model.Id = Id;
                    await s.Delete(model);
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }

        [HttpPost]
        public async Task<JsonResult> LookUp_Role(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<User_LookUpRole> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("UserIdentity")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "Name"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Filter":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@Filter", $"%{x.Value}%"));
                                wheres.Add($"Name LIKE @Filter");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            AspNetRoles
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<User_LookUpRole>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> LookUp_Dokter(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<User_LookupDokter> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "DokterID",
                        "NamaDOkter",
                        "Alamat",
                        "NoKontak",
                        "Active"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Nama":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@Nama", $"%{x.Value}%"));
                                wheres.Add($"NamaDOkter LIKE @Nama");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            mDokter
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<User_LookupDokter>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPost]
        [ActionName("ActionGenerateUser")]
        public async Task<JsonResult> Generate_User()
        {
            try
            {
                List<User_UserSetupModel> modelList = new();

                using (var s = new MahasConnection(Config.GetConnectionString("UserIdentity")))
                {
                    modelList = await s.GetDatas<User_UserSetupModel>("select NIK AS UserName, '01' AS[Password] from dbo.TmpUser where NIK NOT IN (SELECT UserName From AspNetUsers)");

                    foreach (var model in modelList)
                    {
                        var user = new ApplicationUser
                        {
                            UserName = model.UserName,
                            Email = model.Email,
                        };
                        var result = await _userManager.CreateAsync(user, model.Password);
                        if (!result.Succeeded)
                        {
                            var errors = new List<string>();
                            foreach (var error in result.Errors)
                            {
                                errors.Add(error.Description);
                            }
                            throw new Exception(string.Join(", ", errors));
                        }
                    }
                }

                return Json(new
                {
                    Success = true,
                    Data = modelList
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }

        }
    }
}