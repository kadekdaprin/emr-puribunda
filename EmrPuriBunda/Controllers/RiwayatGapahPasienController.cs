using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace EmrPuriBunda.Controllers
{
    // RoleX
    public class RiwayatGapahPasienController : BaseController
    {
        public RiwayatGapahPasienController(IConfiguration config) : base(config)
        {
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Datas_Gapah(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<RiwayatGapahPasien_ListModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "G",
                        "P",
                        "A",
                        "H",
                        "TP",
                        "HPHT",
                        "Keterangan",
                        "LastUpdate",
                        "Tanggal"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Filter":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@Filter", $"{x.Value}"));
                                wheres.Add($"(NRM =@Filter)");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            trRiwayatGapahPasien
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<RiwayatGapahPasien_ListModel>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpGet]
        [ActionName("GPAH")]
        public async Task<JsonResult> Data_GPAH(string nrm)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                RiwayatGapahPasien_SetupModel data;
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var query = $@"
                        SELECT
                            TOP 1
                            *
                        FROM
                            Vw_trRiwayatGapahPasien
                        WHERE
                            nrm=@nrm
                        ORDER BY
                            Tanggal DESC
                    ";

                    data = await s.GetData<RiwayatGapahPasien_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@nrm", nrm),
                    });

                    if (data != null)
                    {
                        var queryDetail = "SELECT * FROM Vw_trRiwayatPersalinan WHERE NRM = @NRM ORDER BY NoUrut, TahunPersalinan";

                        data.RiwayatPersalinan = await s.GetDatas<RiwayatPersalinan_SetupModel>(queryDetail, new List<SqlParameter>
                        {
                            new SqlParameter("@NRM", nrm),
                        });
                    }
                }
                if (data == null) data = new RiwayatGapahPasien_SetupModel();
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        // RoleX
        [HttpPost]
        [ActionName("GPAH")]
        public async Task<JsonResult> Post_GPAH(RiwayatGapahPasien_SetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    s.OpenTransaction();
                    model.ID = await s.Insert(model, true);

                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }        // RoleX
        [HttpPut]
        [ActionName("GPAH")]
        public async Task<JsonResult> Put_GPAH(RiwayatGapahPasien_SetupModel model, int? ID = null)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    model.Tanggal = DateTime.Now;
                    model.LastUpdate = DateTime.Now;
                    model.AspnetUserID = userid;

                    s.OpenTransaction();

                    if (ID != null || model.ID != 0)
                    {
                        await s.Update(model);
                    }
                    else
                    {
                        await s.Insert(model);
                    }

                    await s.ExecuteNonQuery("DELETE FROM trRiwayatPersalinan WHERE NRM = @NRM", new List<SqlParameter>()
                    {
                        new SqlParameter("@NRM", model.NRM)
                    });

                    foreach (var item in (model.RiwayatPersalinan ?? new List<RiwayatPersalinan_SetupModel>()).Select((v, i) => (v, i)))
                    {
                        item.v.NRM = model.NRM;
                        item.v.DibuatTanggal = DateTime.Now;
                        item.v.UserId = userid;
                        item.v.NoUrut = item.i;

                        await s.Insert(item.v);
                    }

                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }        // RoleX
        [HttpDelete]
        [ActionName("GPAH")]
        public async Task<JsonResult> Delete_GPAH(int ID)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    var model = new RiwayatGapahPasien_SetupModel();

                    s.OpenTransaction();

                    model.ID = ID;
                    await s.Delete(model);
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }

        [HttpPost]
        public async Task<JsonResult> Select_Gpah(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Deskripsi LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            KodeGapah AS Value,
                            Deskripsi AS Text
                        FROM
                            mGapah
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }
    }
}