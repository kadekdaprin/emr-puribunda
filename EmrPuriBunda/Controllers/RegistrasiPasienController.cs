using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using EmrPuriBunda.Mahas.Helpers;
using EmrPuriBunda.Constant;
using Microsoft.Extensions.Options;

namespace EmrPuriBunda.Controllers
{
    // RoleX
    [Authorize(Policy = PolicyConstant.Riwayat)]
    public class RegistrasiPasienController : BaseController
    {
        private readonly AppSettings appSettings;

        public RegistrasiPasienController(IConfiguration config, IOptions<AppSettings> options) : base(config)
        {
            appSettings = options.Value;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Datas_RegistrasiPasien(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var section = Request.GetSection();

                if (section == null) throw new Exception("Section login belum terisi.");

                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

                int totalRowCount;
                List<RegistrasiPasien_DataReg> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var wheres = new List<string>()
                    {
                        "SectionID = @SectionID"
                    };

                    var parameters = new List<SqlParameter>()
                    {
                        new SqlParameter("@SectionID", section.SectionID)
                    };

                    var _orderBy = new List<string>()
                    {
                        "NoReg",
                        "NRM",
                        "NamaPasien",
                        "Tanggal",
                        "NamaDOkter",
                        "SudahPeriksa"
                    };

                    var additionalJoin = "";

                    if (User.IsRoleDokterSpesialis())
                    {
                        var dokter = await GetDokterAsync(userid);

                        additionalJoin = @"
                            LEFT JOIN (
                                SELECT 
                                    DokterID AS RegisterDpjp_DokterID, 
                                    NoReg AS RegisterDpjp_NoReg 
                                FROM 
                                    trRegisterDokterDPJPStatus 
                                WHERE 
                                    DokterID = @DokterID  GROUP BY DokterID, NoReg
                            ) RegisterDokter 
                                ON RegisterDokter.RegisterDpjp_NoReg = VW_DataPasienReg.NoReg";

                        wheres.Add("(DokterID = @DokterID OR RegisterDpjp_DokterID = @DokterID)");
                        parameters.Add(new SqlParameter("@DokterID", dokter.DokterID));
                    }

                    var periode = filter.FirstOrDefault(x => x.Key == "Periode" && x.Value == "false");

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "DariTanggal":
                                if (periode != null) break;
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@DariTanggal", $"{x.Value}"));
                                wheres.Add($"Tanggal >= @DariTanggal");
                                break;
                            case "SampaiTanggal":
                                if (periode != null) break;
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@SampaiTanggal", $"{x.Value}"));
                                wheres.Add($"Tanggal <= @SampaiTanggal");
                                break;
                            case "Status":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@Status", $"{x.Value}"));
                                wheres.Add($"SudahPeriksa = @Status");
                                break;
                            case "NoReg":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@NoReg", $"%{x.Value}%"));
                                wheres.Add($"NoReg LIKE @NoReg");
                                break;
                            case "NamaDokter":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@NamaDokter", $"%{x.Value}%"));
                                wheres.Add($"NamaDokter LIKE @NamaDokter");
                                break;
                            case "NamaPasien":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@NamaPasien", $"%{x.Value}%"));
                                wheres.Add($"(NamaPasien LIKE @NamaPasien)");
                                break;
                            case "NRM":
                                if (string.IsNullOrEmpty(x.Value) || x.Value == "__.__.__") break;
                                parameters.Add(new SqlParameter($"@NRM", $"{x.Value}"));
                                wheres.Add($"(NRM = @NRM)");
                                break;
                            case "SudahIsiAsesmen":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                if (section.GroupPelayanan == appSettings.DefaultConfig.GroupPelayananRI)
                                {
                                    wheres.Add($"AsesmenRI = @SudahIsiAsesmen");
                                }
                                if (section.GroupPelayanan == appSettings.DefaultConfig.GroupPelayananRJ)
                                {
                                    wheres.Add($"AsesmenRJ = @SudahIsiAsesmen");
                                }
                                if (section.GroupPelayanan == appSettings.DefaultConfig.GroupPelayananUGD)
                                {
                                    wheres.Add($"AsesmenUGD = @SudahIsiAsesmen");
                                }
                                parameters.Add(new SqlParameter($"@SudahIsiAsesmen", x.Value));
                                break;
                            case "SudahCheckout":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@SudahCheckout", x.Value));
                                wheres.Add($"SudahCheckout = @SudahCheckout");
                                break;
                            case "SudahDisposisi":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@SudahDisposisi", x.Value));
                                wheres.Add($"COALESCE(Disposisi, 0) = @SudahDisposisi");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            VW_DataPasienReg
                            {additionalJoin}
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<RegistrasiPasien_DataReg>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters, ", Tanggal DESC, NamaDokter ASC, NoAntri ASC");
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpGet]
        [ActionName("ActionRegistrasiPasien")]
        public async Task<JsonResult> Data_RegistrasiPasien(string noReg)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                RegistrasiPasien_RegistrasiPasienSetupModel data;
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            VW_DataPasienReg
                        WHERE
                            NoReg=@NoReg
                    ";

                    data = await s.GetData<RegistrasiPasien_RegistrasiPasienSetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoReg", noReg),
                    });


                }
                if (data == null) throw new Exception("Data tidak ditemukan");
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }
    }
}