using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using EmrPuriBunda.Mahas.Helpers;
using EmrPuriBunda.Constant;

namespace EmrPuriBunda.Controllers
{
    [Authorize(Policy = PolicyConstant.EResep)]
    public class ResepController : BaseController
    {
        public ResepController(IConfiguration config) : base(config)
        {
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Datas_Resep(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<Resep_Resep_Datas> datas;

                var dokter = await GetDokterAsync(userid);
                var section = Request.GetSection();

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var wheres = new List<string>();
                    var parameters = new List<SqlParameter>();
                    var _orderBy = new List<string>()
                    {
                        "NoRegistrasi",
                        "Tanggal"
                    };

                    if (User.IsRoleAdmin() == false && User.IsRolePerawatBidan() == false)
                    {
                        wheres.Add("(DokterID = @DokterID OR AspNetUserId = @UserId)");
                        parameters.Add(new SqlParameter("@DokterID", (object)dokter?.DokterID ?? DBNull.Value));
                        parameters.Add(new SqlParameter("@UserId", userid));
                    }

                    wheres.Add("SectionID = @SectionID");
                    parameters.Add(new SqlParameter("@SectionID", section.SectionID));

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "NoReg":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@NoReg", x.Value));
                                wheres.Add($"(NoRegistrasi = @NoReg)");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            EResep_HeaderDetail
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);

                    datas = await s.GetDatas<Resep_Resep_Datas>(query, _orderBy[orderBy], orderByType,  pageIndex, pageSize, parameters);
                }

                var result = datas.GroupBy(x => new {
                    x.NoRegistrasi
                    , x.NoResep
                    , x.Tanggal
                    , x.Jam
                    , x.DokterID
                    , x.NamaDokter
                    , x.Farmasi_SectionID
                    , x.SectionFarmasi
                    , x.KodePaket
                    , x.NamaPaket
                    , x.Cyto
                    , x.Puyer
                    , x.QtyPuyer
                    , x.SatuanPuyer
                    , x.Keterangan
                    , x.Realisasi
                    , x.SedangProses
                    , x.SedangProsesNamaUser
                    , x.BeratBadan
                    , x.Batal
                }).Select(y => new Resep_Resep_Header()
                {
                    NoRegistrasi = y.Key.NoRegistrasi,
                    NoResep = y.Key.NoResep,
                    Tanggal = y.Key.Tanggal,
                    Jam = y.Key.Jam,
                    DokterID = y.Key.DokterID,
                    NamaDokter = y.Key.NamaDokter,
                    Farmasi_SectionID = y.Key.Farmasi_SectionID,
                    SectionFarmasi = y.Key.SectionFarmasi,
                    KodePaket = y.Key.KodePaket,
                    NamaPaket = y.Key.NamaPaket,
                    Cyto = y.Key.Cyto,
                    Puyer = y.Key.Puyer,
                    QtyPuyer = y.Key.QtyPuyer,
                    SatuanPuyer = y.Key.SatuanPuyer,
                    Keterangan = y.Key.Keterangan,
                    Realisasi = y.Key.Realisasi,
                    SedangProses = y.Key.SedangProses,
                    SedangProsesNamaUser = y.Key.SedangProsesNamaUser,
                    BeratBadan = y.Key.BeratBadan.ToString(),
                    Batal = y.Key.Batal,
                    ResepDetail = y.Select(z => new Resep_Resep_Detail(z)).ToList()
                });

                return Json(new
                {
                    Success = true,
                    Datas = result,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpGet]
        [ActionName("Resep")]
        public async Task<JsonResult> Data_Resep(string NoResep)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                Resep_Resep_GetData data;
                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            EResep_HeaderResep
                        WHERE
                            NoResep=@NoResep
                    ";

                    data = await s.GetData<Resep_Resep_GetData>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoResep", NoResep),
                    });

                    if (data == null) throw new Exception("Data tidak ditemukan");

                    query = $@"						SELECT							*						FROM							EResep_DetailResep						WHERE                            NoResep=@NoResep												";

                    data.table1 = await s.GetDatas<Resep_Resep_table1_GetData>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoResep", NoResep)
                    });

                    data.HeaderAturan1 = data.table1?.FirstOrDefault()?.Dosis;
                    data.HeaderAturan2 = data.table1?.FirstOrDefault()?.Dosis2;
                }
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        // RoleX
        [HttpPost]
        [ActionName("Resep")]
        public async Task<JsonResult> Post_Resep(Resep_Resep_GetData model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

            if (!ModelState.IsValid)
            {
                return InvalidModelResult();
            }

            using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
            {
                try
                {
                    if (string.IsNullOrEmpty(model.NoRegistrasi)) throw new Exception("No Registrasi harus diisi");

                    if (model.table1 == null) throw new Exception("List obat harus diisi");

                    var section = Request.GetSection();

                    model.SectionID = section?.SectionID;
                    model.SectionAwalName = section.SectionName;
                    model.Jam = model.Tanggal;

                    model.Puyer = model.QtyPuyer != null || model.QtyPuyer > 0;

                    if (model.Puyer == true)
                    {
                        model.table1?.ForEach(x =>
                        {
                            x.Dosis = model.HeaderAturan1;
                            x.Dosis2 = model.HeaderAturan2;
                            x.Qty = 1;
                        });
                    }

                    s.OpenTransaction();

                    model.NoResep = await s.ExecuteScalar<string>(
                        @"exec EResep_insert_New 
                                @NoRegistrasi
                                , @SectionID
                                , @Tanggal
                                , @Jam
                                , @DokterID
                                , @Jumlah
                                , @Cyto
                                , @Farmasi_SectionID
                                , @Paket
                                , @KodePaket
                                , @Keterangan
                                , @Puyer
                                , @QtyPuyer
                                , @SatuanPuyer
                                , @BeratBadan
                                , @AspNetUserId", 
                        new List<SqlParameter>
                        {
                            new SqlParameter("@NoRegistrasi", (object) model.NoRegistrasi ?? DBNull.Value),
                            new SqlParameter("@SectionID", (object) model.SectionID ?? DBNull.Value),
                            new SqlParameter("@Tanggal", (object) model.Tanggal ?? DBNull.Value),
                            new SqlParameter("@Jam", (object) model.Jam ?? DBNull.Value),
                            new SqlParameter("@DokterID", (object) model.DokterID ?? DBNull.Value),
                            new SqlParameter("@Jumlah", (object) model.Jumlah ?? DBNull.Value),
                            new SqlParameter("@Cyto", (object) model.Cyto ?? DBNull.Value),
                            new SqlParameter("@Farmasi_SectionID", (object) model.Farmasi_SectionID ?? DBNull.Value),
                            new SqlParameter("@Paket", (object) model.Paket ?? DBNull.Value),
                            new SqlParameter("@KodePaket", (object) model.KodePaket ?? DBNull.Value),
                            new SqlParameter("@Keterangan", (object) model.Keterangan ?? DBNull.Value),
                            new SqlParameter("@Puyer", (object) model.Puyer ?? DBNull.Value),
                            new SqlParameter("@QtyPuyer", (object) model.QtyPuyer ?? DBNull.Value),
                            new SqlParameter("@SatuanPuyer", (object) model.SatuanPuyer ?? DBNull.Value),
                            new SqlParameter("@BeratBadan", (object) model.BeratBadan ?? DBNull.Value),
                            new SqlParameter("@AspNetUserId", (object) userid ?? DBNull.Value)
                        });

                    await s.ExecuteNonQuery("UPDATE SIMtrRegistrasi SET Beratbadan = @BeratBadan WHERE NoReg = @NoRegistrasi", new List<SqlParameter>()
                    {
                        new SqlParameter("@NoRegistrasi", model.NoRegistrasi),
                        new SqlParameter("@BeratBadan", model.BeratBadan)
                    });

                    foreach (var item in model.table1.Select((x, i) => (x, i)))
                    {
                        //var paramstring = $@"
                        //    @NoResep = {model.NoResep},
                        //    @Barang_ID = {item.x.Barang_ID},
                        //    @Satuan = {item.x.Satuan},
                        //    @Qty = {item.x.Qty},
                        //    @Harga_Satuan = {item.x.Harga_Satuan},
                        //    @Stok = {item.x.Stok},
                        //    @DosisID = {item.x.DosisID},
                        //    @JenisBarangID = {item.x.JenisBarangID},
                        //    @Dosis = {item.x.Dosis},
                        //    @NomorUrut = {item.i},
                        //    @KetDosis = {item.x.KetDosis},
                        //    @Dosis2 = {item.x.Dosis2}
                        //";

                        await s.ExecuteNonQuery(@" EXEC EResep_Detail_insert
                            @NoResep
                            , @Barang_ID
                            , @Satuan
                            , @Qty
                            , @Harga_Satuan
                            , @Stok
                            , @DosisID
                            , @JenisBarangID
                            , @Dosis
                            , @NomorUrut
                            , @KetDosis
                            , @Dosis2",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@NoResep", (object) model.NoResep ?? DBNull.Value),
                                new SqlParameter("@Barang_ID", (object) item.x.Barang_ID ?? DBNull.Value),
                                new SqlParameter("@Satuan", (object) item.x.Satuan ?? DBNull.Value),
                                new SqlParameter("@Qty", (object) item.x.Qty ?? DBNull.Value),
                                new SqlParameter("@Harga_Satuan", (object) item.x.Harga_Satuan ?? DBNull.Value),
                                new SqlParameter("@Stok", (object) item.x.Stok ?? DBNull.Value),
                                new SqlParameter("@DosisID", (object) item.x.DosisID ?? DBNull.Value),
                                new SqlParameter("@JenisBarangID", (object) item.x.JenisBarangID ?? DBNull.Value),
                                new SqlParameter("@Dosis", (object) item.x.Dosis ?? DBNull.Value),
                                new SqlParameter("@NomorUrut", DBNull.Value),
                                new SqlParameter("@KetDosis", (object) item.x.KetDosis ?? DBNull.Value),
                                new SqlParameter("@Dosis2", (object) item.x.Dosis2 ?? DBNull.Value),
                            });
                    }
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }        // RoleX
        [HttpPut]
        [ActionName("Resep")]
        public async Task<JsonResult> Put_Resep(string NoResep, Resep_Resep_GetData model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

            if (!ModelState.IsValid)
            {
                return InvalidModelResult();
            }

            using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
            {
                try
                {
                    if (model.table1 == null) throw new Exception("List obat harus diisi");

                    var oldModel = new Resep_Resep_SetupModel();

                    var queryHeader = "";
                    queryHeader = $@"						SELECT							*						FROM							SIMtrResep						WHERE                            NoResep = @NoResep					";
                    oldModel = await s.GetData<Resep_Resep_SetupModel>(queryHeader, new List<SqlParameter>
                    {
                        new SqlParameter("@NoResep", NoResep)
                    });

                    if (oldModel.Realisasi) throw new Exception("Obat sudah di realisasi. Tidak dapat diubah.");
                    if (oldModel.SedangProses == true) throw new Exception("Obat sedang diproses. Tidak dapat diubah.");

                    var query = "";
                    query = $@"						SELECT							*						FROM							SIMtrResepDetail						WHERE                            NoResep = @NoResep					";
                    oldModel.table1 = await s.GetDatas<Resep_Resep_table1_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoResep", NoResep)
                    });

                    s.OpenTransaction();

                    //TODO : Isi field2 header yang boleh di update
                    oldModel.Farmasi_SectionID = model.Farmasi_SectionID;
                    oldModel.DokterID = model.DokterID;
                    oldModel.Cyto = model.Cyto;
                    oldModel.BeratBadan = model.BeratBadan;
                    oldModel.QtyPuyer = model.QtyPuyer;
                    oldModel.SatuanPuyer = model.SatuanPuyer;
                    oldModel.Keterangan = model.Keterangan;

                    if (oldModel.Puyer == true)
                    {
                        model.table1?.ForEach(x =>
                        {
                            x.Dosis = model.HeaderAturan1;
                            x.Dosis2 = model.HeaderAturan2;
                            x.Qty = 1;
                        });
                    }

                    // Update Header
                    await s.Update(oldModel);

                    await s.ExecuteNonQuery("UPDATE SIMtrRegistrasi SET Beratbadan = @BeratBadan WHERE NoReg = @NoRegistrasi", new List<SqlParameter>()
                    {
                        new SqlParameter("@NoRegistrasi", oldModel.NoRegistrasi),
                        new SqlParameter("@BeratBadan", oldModel.BeratBadan)
                    });

                    foreach (var x in oldModel.table1)
                    {
                        if (model.table1.FirstOrDefault(y =>
                            y.Barang_ID == x.Barang_ID
                        ) == null)
                        {
                            await s.Delete(x);
                        }
                    }
                    foreach (var newOne in model.table1)
                    {
                        var oldOne = oldModel.table1.FirstOrDefault(y => y.Barang_ID == newOne.Barang_ID);
                        if (oldOne == null)
                        {
                            await s.ExecuteNonQuery(@" EXEC EResep_Detail_insert
                            @NoResep
                            , @Barang_ID
                            , @Satuan
                            , @Qty
                            , @Harga_Satuan
                            , @Stok
                            , @DosisID
                            , @JenisBarangID
                            , @Dosis
                            , @NomorUrut
                            , @KetDosis
                            , @Dosis2",
                            new List<SqlParameter>()
                            {
                                new SqlParameter("@NoResep", (object) model.NoResep ?? DBNull.Value),
                                new SqlParameter("@Barang_ID", (object) newOne.Barang_ID ?? DBNull.Value),
                                new SqlParameter("@Satuan", (object) newOne.Satuan ?? DBNull.Value),
                                new SqlParameter("@Qty", (object) newOne.Qty ?? DBNull.Value),
                                new SqlParameter("@Harga_Satuan", (object) newOne.Harga_Satuan ?? DBNull.Value),
                                new SqlParameter("@Stok", (object) newOne.Stok ?? DBNull.Value),
                                new SqlParameter("@DosisID", (object) newOne.DosisID ?? DBNull.Value),
                                new SqlParameter("@JenisBarangID", (object) newOne.JenisBarangID ?? DBNull.Value),
                                new SqlParameter("@Dosis", (object) newOne.Dosis ?? DBNull.Value),
                                new SqlParameter("@NomorUrut", DBNull.Value),
                                new SqlParameter("@KetDosis", (object) newOne.KetDosis ?? DBNull.Value),
                                new SqlParameter("@Dosis2", (object) newOne.Dosis2 ?? DBNull.Value),
                            });
                        }
                        else
                        {
                            oldOne.Dosis = newOne.Dosis;
                            oldOne.Dosis2 = newOne.Dosis2;
                            oldOne.Qty = newOne.Qty;
                            oldOne.KetDosis = newOne.KetDosis;

                            await s.Update(oldOne);
                        }
                    }
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }        // RoleX
        [HttpDelete]
        [ActionName("Resep")]
        public async Task<JsonResult> Delete_Resep(string NoResep)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
            {
                try
                {
                    var oldModel = new Resep_Resep_SetupModel();

                    var queryHeader = "";
                    queryHeader = $@"						SELECT							*						FROM							SIMtrResep						WHERE                            NoResep = @NoResep					";
                    oldModel = await s.GetData<Resep_Resep_SetupModel>(queryHeader, new List<SqlParameter>
                    {
                        new SqlParameter("@NoResep", NoResep)
                    });

                    if (oldModel.Realisasi) throw new Exception("Obat sudah di realisasi. Tidak dapat diubah.");
                    if (oldModel.SedangProses == true) throw new Exception("Obat sedang diproses. Tidak dapat diubah.");

                    s.OpenTransaction();

                    await s.ExecuteNonQuery("exec EResep_SIMtrResep_Batal @NoResep", new List<SqlParameter>()
                    {
                        new SqlParameter("@NoResep", NoResep)
                    });

                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }        [HttpGet]
        [ActionName("TemplateDosis")]
        public async Task<JsonResult> TemplateDosis(string query)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                List<Resep_DosisObat> data;
                var parameters = new List<SqlParameter>();
                var wheres = new List<string>();

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    if (!string.IsNullOrEmpty(query))
                    {
                        parameters.Add(new SqlParameter("@Dosis", $"%{query.Replace(" ", "")}%"));
                        wheres.Add(@"REPLACE(Dosis , ' ', '') LIKE @Dosis");
                    }

                    var q = $@"
                        SELECT Dosis FROM SIMmDosisObat {s.ToWhere(wheres)}
                    ";

                    data = await s.GetDatas<Resep_DosisObat>(q, parameters);
                }
                if (data == null) throw new Exception("Data tidak ditemukan");

                return Json(data.Select(x => x.Dosis).ToList());
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPost]
        public async Task<JsonResult> EMR_Select_SectionFarmasi(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            EMR_Select_SectionFarmasi
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPost]
        public async Task<JsonResult> EMR_Select_SediaanRacik(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            NamaRacikan AS [Value],
                            NamaRacikan AS [Text]
                        FROM
                            SIMmRacikan
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPost]
        public async Task<JsonResult> EMR_Select_Dokter(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("NamaDOkter LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            DokterID AS [Value]
                            , NamaDOkter AS [Text]
                        FROM
                            Vw_Dokter
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        //Lookup Paket Obat        [HttpPost]
        public async Task<JsonResult> LookUp_lookup1(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

                var dokter = await GetDokterAsync(userid);

                var section = Request.GetSection();

                int totalRowCount;
                List<Resep_Resep_lookup1_LookUpModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "KodePaket",
                        "NamaPaket"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Filter":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@Filter", $"%{x.Value}%"));
                                wheres.Add($"NamaPaket LIKE @Filter");
                                break;
                            case "Racik":
                                parameters.Add(new SqlParameter($"@Racik", (object)x.Value ?? DBNull.Value));
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            EMR_EResep_GetListPaketObat(@DokterID, @SectionID, @Racik)
                        {s.ToWhere(wheres)}
                    ";

                    parameters.Add(new SqlParameter("@DokterID", (object)dokter.DokterID ?? DBNull.Value));
                    parameters.Add(new SqlParameter("@SectionID", (object)section.SectionID ?? DBNull.Value));

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<Resep_Resep_lookup1_LookUpModel>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPost]
        public async Task<JsonResult> LookUp_table1(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<Resep_Resep_table1_LookUpModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "NamaBarang",
                        "Satuan_Stok",
                        "Qty_Stok",
                        "Barang_ID"
                    };                    

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "NoReg":
                                parameters.Add(new SqlParameter("@NoReg", x.Value));
                                break;
                            case "SectionID":
                                parameters.Add(new SqlParameter("@SectionID", x.Value));
                                break;
                            case "NamaObat":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@NamaObat", $"%{x.Value}%"));
                                wheres.Add($"(NamaBarang LIKE @NamaObat OR NamaGenerik LIKE @NamaObat)");
                                break;
                            case "SemuaObat":
                                parameters.Add(new SqlParameter($"@SemuaObat", x.Value));
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Pelayanan_GetListObat(@SectionID, @NoReg, @SemuaObat)
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<Resep_Resep_table1_LookUpModel>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpGet]
        public async Task<JsonResult> AutoId_autoid1()
        {
            try
            {
                var id = await GetNoBukti_AutoIdAsync();

                return Json(new
                {
                    Success = true,
                    Id = id
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        private async Task<string> GetNoBukti_AutoIdAsync()
        {
            using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
            {
                var dateCode = "yyyyMMdd";
                var baseCode = "@{DATE}ERESEP-######";
                var tempCode = MahasConverter.AutoIdGetTempCode(baseCode, dateCode);

                var parameters = new List<SqlParameter>()
                    {
                        new SqlParameter("@NoResep", $"%{tempCode}%")
                    };
                var wheres = new List<string>()
                    {
                        "NoResep LIKE @NoResep"
                    };

                var query = $@"
                        SELECT
                            *
                        FROM
                            SIMtrResep
                        {s.ToWhere(wheres)}
                        ORDER BY NoResep DESC
                    ";

                var data = await s.GetData<Resep_Resep_SetupModel>(query, parameters);
                string id = MahasConverter.AutoId(baseCode, dateCode, data?.NoResep);

                return id;
            }
        }

        [HttpPost]
        public async Task<JsonResult> PaketObatDetail(string kodePaket, string sectionID, string noReg)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

                List<Resep_Resep_table1_GetData> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var query = $@"
                        SELECT
	                        Barang_ID  = Obat.Barang_ID
	                        , Satuan = Obat.Satuan_Stok
	                        , Qty = Paket.Qty
	                        , Harga_Satuan = Obat.Harga_Jual
	                        , Stok   = Obat.Qty_Stok
	                        , DosisID = null
	                        , JenisBarangID = null
	                        , Dosis = Paket.AturanPakai
	                        , NomorUrut = null
	                        , KetDosis = Paket.KetDosis
	                        , Dosis2 = null
	                        , Nama_Barang = Obat.NamaBarang

                        FROM
                            SIMmDetailPaketObat Paket
	                        INNER JOIN (select * from Pelayanan_GetListObat(@SectionID, @NoReg, @SemuaObat)) Obat ON Obat.Barang_ID = Paket.Barang_ID
                        WHERE 
	                        Paket.KodePaket = @KodePaket
                    ";

                    datas = await s.GetDatas<Resep_Resep_table1_GetData>(query, new List<SqlParameter>()
                    {
                        new SqlParameter("@SectionID", sectionID),
                        new SqlParameter("@NoReg", noReg),
                        new SqlParameter("@KodePaket", kodePaket),
                        new SqlParameter("@SemuaObat", true)
                    });
                }

                return Json(new
                {
                    Success = true,
                    Data = datas
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }
    }
}