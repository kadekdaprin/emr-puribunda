using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using EmrPuriBunda.Constant;

namespace EmrPuriBunda.Controllers
{
    [Authorize(Policy = PolicyConstant.SetupMaster)]
    public class TriageController : Controller
    {
        private IConfiguration Config { get; set; }
        public TriageController(IConfiguration configuration) => Config = configuration;

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Datas_Triage(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<Triage_TriageModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "Triage"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Filter":								if (string.IsNullOrEmpty(x.Value)) break;								parameters.Add(new SqlParameter($"@Filter", $"%{x.Value}%"));								wheres.Add($"(Triage LIKE @Filter)");								break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            mTriage
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<Triage_TriageModel>(query, _orderBy[orderBy], orderByType,  pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpGet]
        [ActionName("ActionTriage")]
        public async Task<JsonResult> Data_Triage(int Id)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                Triage_TriageSetupModel data;
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            mTriage
                        WHERE
                            Id=@Id
                    ";

                    data = await s.GetData<Triage_TriageSetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@Id", Id),
                    });

                    
                }
                if (data == null) throw new Exception("Data tidak ditemukan");
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        // RoleX
        [HttpPost]
        [ActionName("ActionTriage")]
        public async Task<JsonResult> Post_Triage(Triage_TriageSetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    s.OpenTransaction();
                    model.Id = await s.Insert(model, true);
                    
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }        // RoleX
        [HttpPut]
        [ActionName("ActionTriage")]
        public async Task<JsonResult> Put_Triage(int Id, Triage_TriageSetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    

                    s.OpenTransaction();
                    model.Id = Id;
                    await s.Update(model);

                    
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }        // RoleX
        [HttpDelete]
        [ActionName("ActionTriage")]
        public async Task<JsonResult> Delete_Triage(int Id)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    var model = new Triage_TriageSetupModel();
                    

                    s.OpenTransaction();
                    
                    model.Id = Id;
                    await s.Delete(model);
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }
    }
}