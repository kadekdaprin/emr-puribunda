using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using EmrPuriBunda.Mahas.Helpers;
using Microsoft.AspNetCore.Hosting;
using System.Data;
using AspNetCore.Reporting;

namespace EmrPuriBunda.Controllers
{
    // RoleX
    public class DischargeSummaryController : BaseController
    {
        private readonly IWebHostEnvironment WebHostEnvt;

        public DischargeSummaryController(IConfiguration config, IWebHostEnvironment webHostEnvt) : base(config)
        {
            WebHostEnvt = webHostEnvt;
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
        }

        [HttpGet]
        [ActionName("DischargeSummary")]
        public async Task<JsonResult> Data_DischargeSummary(string NoReg)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                DischargeSummary_SetupModel data;
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_DischargeSummary
                        WHERE
                            NoReg=@NoReg
                    ";

                    data = await s.GetData<DischargeSummary_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoReg", NoReg),
                    });

                    query = $@"
						SELECT
							*
						FROM
							Vw_DischargeSummary_DiagnosaSekunder
						WHERE
							NoReg=@NoReg
                        ORDER BY 
                            NoUrut
					";

                    if (data != null)
                    {
                        data.tableDiagnosa = await s.GetDatas<DischargeSummary_DiagnosaSekunder>(query, new List<SqlParameter>
                        {
                            new SqlParameter("@NoReg", NoReg)
                        });
                    }
                }
                if (data == null) throw new Exception("Data tidak ditemukan");
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        // RoleX
        [HttpPost]
        [ActionName("DischargeSummary")]
        public async Task<JsonResult> Post_DischargeSummary(DischargeSummary_SetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

            var dokter = await GetDokterAsync();
            var section = Request.GetSection();

            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    model.UserId = userid;
                    model.DokterId = dokter.DokterID;

                    if (!User.IsRoleDokterSpesialis() && !User.IsRoleAdmin())
                    {
                        model.SimpanSementara = true;
                    }

                    s.OpenTransaction();

                    await s.Insert(model);

                    if (model.tableDiagnosa == null) model.tableDiagnosa = new List<DischargeSummary_DiagnosaSekunder>();
                    foreach (var (x, i) in model.tableDiagnosa.Select((y, i) => (y, i)))
                    {
                        var detailModel = new DischargeSummary_DiagnosaSekunder()
                        {
                            NoUrut = i + 1,
                            NoReg = model.NoReg,
                            KodeICD = x.KodeICD
                        };

                        await s.Insert(detailModel);
                    }

                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }        // RoleX
        [HttpPut]
        [ActionName("DischargeSummary")]
        public async Task<JsonResult> Put_DischargeSummary(string NoReg, DischargeSummary_SetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var dokter = await GetDokterAsync();
            var section = Request.GetSection();

            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    model.UserId = userid;
                    model.DokterId = dokter.DokterID;

                    if (!User.IsRoleAdmin() && !User.IsRoleDokterSpesialis())
                    {
                        model.SimpanSementara = true;
                    }

                    var oldModel = new DischargeSummary_SetupModel();
                    var queryOldModel = $@"
						SELECT
							*
						FROM
							DischargeSummary
						WHERE
							NoReg=@NoReg";

                    oldModel = await s.GetData<DischargeSummary_SetupModel>(queryOldModel, new List<SqlParameter> { new SqlParameter("@NoReg", NoReg) });

                    var query = "";
                    query = $@"
						SELECT
							*
						FROM
							DischargeSummary_DiagnosaSekunder
						WHERE
							NoReg=@NoReg
					";
                    oldModel.tableDiagnosa = await s.GetDatas<DischargeSummary_DiagnosaSekunder>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoReg", NoReg)
                    });

                    s.OpenTransaction();

                    model.NoReg = oldModel.NoReg;

                    await s.Update(model);

                    foreach (var x in oldModel.tableDiagnosa)
                    {
                        await s.Delete(x);
                    }

                    model.tableDiagnosa ??= new List<DischargeSummary_DiagnosaSekunder>();

                    foreach (var (x, i) in model.tableDiagnosa.Select((y, i) => (y, i)))
                    {
                        var detailModel = new DischargeSummary_DiagnosaSekunder()
                        {
                            NoUrut = i + 1,
                            NoReg = model.NoReg,
                            KodeICD = x.KodeICD
                        };

                        await s.Insert(detailModel);
                    }

                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }        // RoleX
        [HttpDelete]
        [ActionName("DischargeSummary")]
        public async Task<JsonResult> Delete_DischargeSummary(string NoReg)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    var model = new DischargeSummary_SetupModel();
                    var query = "";
                    query = $@"
						SELECT
							*
						FROM
							DischargeSummary_DiagnosaSekunder
						WHERE
							NoReg=@NoReg
					";
                    model.tableDiagnosa = await s.GetDatas<DischargeSummary_DiagnosaSekunder>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoReg", NoReg)
                    });

                    s.OpenTransaction();
                    foreach (var x in model.tableDiagnosa) await s.Delete(x);
                    model.NoReg = NoReg;
                    await s.Delete(model);
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }        [HttpGet]
        [ActionName("DiagnosaAwal")]
        public async Task<JsonResult> Data_DiagnosaAwal(string NoReg)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                var section = Request.GetSection();

                MahasSelectListItem data;
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var query = $@"
                        SELECT
                            dbo.Fs_GetDiagnosaAwalAsesmen(@NoReg) AS Text
                    ";

                    data = await s.GetData<MahasSelectListItem>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoReg", NoReg),
                        new SqlParameter("@SectionId", section.SectionID),
                    });
                }
                
                return Json(new
                {
                    Success = true,
                    Data = data?.Text
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPost]
        public async Task<JsonResult> mKesanUmum(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("KesanUmum LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            Id AS Value,
                            KesanUmum AS Text
                        FROM
                            mKesanUmum
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPost]
        public async Task<JsonResult> Vw_Select_CaraKeluar(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Select_CaraKeluar
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> Vw_Select_KondisiKeluar(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Select_KondisiKeluar
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> LookUp_Diagnosa(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<DischargeSummary_DiagnosaLookUpModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "KodeICD",
                        "Descriptions"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Filter":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@Filter", $"%{x.Value}%"));
                                wheres.Add($"(KodeICD LIKE @Filter OR Descriptions LIKE @Filter)");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_ICD
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<DischargeSummary_DiagnosaLookUpModel>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        #region REPORTS
        [HttpGet]
        public IActionResult RptDischargeSummary(string noReg)
        {
            int extension = 1;

            var dt = new DataTable();

            using (SqlConnection con = new SqlConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("Rpt_DischargeSummary", con) 
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.Add(new SqlParameter("@NoReg", noReg));

                    con.Open();

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                }
                catch (Exception)
                {
                    con.Close();
                    throw;
                }
            }

            var path = $"{WebHostEnvt.WebRootPath}\\reports\\rptDischargeSummary.rdlc";

            LocalReport localReport = new LocalReport(path);

            localReport.AddDataSource("dsDischargeSummary", dt);

            var res = localReport.Execute(RenderType.Pdf, extension);

            return File(res.MainStream, "application/pdf");
        }
        #endregion
    }
}