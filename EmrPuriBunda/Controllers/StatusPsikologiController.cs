using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using EmrPuriBunda.Constant;

namespace EmrPuriBunda.Controllers
{
    [Authorize(Policy = PolicyConstant.SetupMaster)]
    public class StatusPsikologiController : Controller
    {
        private IConfiguration Config { get; set; }
        public StatusPsikologiController(IConfiguration configuration) => Config = configuration;

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Datas_StatusPsikologi(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<StatusPsikologi_StatusPsikologiModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "StatusPsikologis"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Filter":								if (string.IsNullOrEmpty(x.Value)) break;								parameters.Add(new SqlParameter($"@Filter", $"%{x.Value}%"));								wheres.Add($"(StatusPsikologis LIKE @Filter)");								break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            mStatusPsikologi
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<StatusPsikologi_StatusPsikologiModel>(query, _orderBy[orderBy], orderByType,  pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpGet]
        [ActionName("ActionStatusPsikologi")]
        public async Task<JsonResult> Data_StatusPsikologi(int Id)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                StatusPsikologi_StatusPsikologiSetupModel data;
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            mStatusPsikologi
                        WHERE
                            Id=@Id
                    ";

                    data = await s.GetData<StatusPsikologi_StatusPsikologiSetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@Id", Id),
                    });

                    
                }
                if (data == null) throw new Exception("Data tidak ditemukan");
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        // RoleX
        [HttpPost]
        [ActionName("ActionStatusPsikologi")]
        public async Task<JsonResult> Post_StatusPsikologi(StatusPsikologi_StatusPsikologiSetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    s.OpenTransaction();
                    model.Id = await s.Insert(model, true);
                    
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }        // RoleX
        [HttpPut]
        [ActionName("ActionStatusPsikologi")]
        public async Task<JsonResult> Put_StatusPsikologi(int Id, StatusPsikologi_StatusPsikologiSetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    

                    s.OpenTransaction();
                    model.Id = Id;
                    await s.Update(model);

                    
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }        // RoleX
        [HttpDelete]
        [ActionName("ActionStatusPsikologi")]
        public async Task<JsonResult> Delete_StatusPsikologi(int Id)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    var model = new StatusPsikologi_StatusPsikologiSetupModel();
                    

                    s.OpenTransaction();
                    
                    model.Id = Id;
                    await s.Delete(model);
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }
    }
}