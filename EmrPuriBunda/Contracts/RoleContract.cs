﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmrPuriBunda.Contracts
{
    public class RoleContract
    {
        public static readonly string Master = "Master";

        public static readonly string Dokter = $"Dokter";

        public static readonly string Perawat = $"Perawat";
    }
}
