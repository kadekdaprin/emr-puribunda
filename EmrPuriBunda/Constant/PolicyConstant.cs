﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmrPuriBunda.Constant
{
    public static class PolicyConstant
    {
        public const string SetupMaster = "SetupMaster";

        public const string AsesmenAwalDokterRJ = "AsesmenAwalDokterRJ";
        public const string AsesmenAwalDokterIGD = "AsesmenAwalDokterIGD";
        public const string AsesmenAwalDokterRI = "AsesmenAwalDokterRI";
        public const string DischargeSummary = "DischargeSummary";

        public const string AsesmenAwalPerawatRJ = "AsesmenAwalPerawatRJ";
        public const string AsesmenAwalPerawatIGD = "AsesmenAwalPerawatIGD";
        public const string AsesmenAwalPerawatRI = "AsesmenAwalPerawatRI";

        public const string Cppt = "Cppt";
        public const string Riwayat = "Riwayat";
        public const string EResep = "E-Resep";
        public const string EPenunjang = "E-Penunjang";
        public const string InputPenunjang = "InputPenunjang";
        public const string InputEDokumen = "InputEDokumen";

        public const string SadurAsesmen = "SadurAsesmen";
    }
}
