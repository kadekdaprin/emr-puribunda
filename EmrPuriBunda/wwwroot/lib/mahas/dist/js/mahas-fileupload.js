﻿const MahasFileUpload = {

    modalTemplate : '<div class="modal-dialog modal-lg" role="document">\n' +
        '  <div class="modal-content">\n' +
        '    <div class="modal-header align-items-center">\n' +
        '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
        '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
        '    </div>\n' +
        '    <div class="modal-body">\n' +
        '      <div class="floating-buttons btn-group"></div>\n' +
        '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
        '    </div>\n' +
        '  </div>\n' +
        '</div>\n',

    // Buttons inside zoom modal
    previewZoomButtonClasses : {
        toggleheader: 'btn btn-light btn-icon btn-header-toggle btn-sm',
        fullscreen: 'btn btn-light btn-icon btn-sm',
        borderless: 'btn btn-light btn-icon btn-sm',
        close: 'btn btn-light btn-icon btn-sm'
    },

    // Icons inside zoom modal classes
    previewZoomButtonIcons : {
        prev: '<i class="icon-arrow-left32"></i>',
        next: '<i class="icon-arrow-right32"></i>',
        toggleheader: '<i class="icon-menu-open"></i>',
        fullscreen: '<i class="icon-screen-full"></i>',
        borderless: '<i class="icon-alignment-unalign"></i>',
        close: '<i class="icon-cross2 font-size-base"></i>'
    },

    // File actions
    fileActionSettings : {
        zoomClass: '',
        zoomIcon: '<i class="icon-zoomin3"></i>',
        dragClass: 'p-2',
        dragIcon: '<i class="icon-three-bars"></i>',
        removeClass: '',
        removeErrorClass: 'text-danger',
        removeIcon: '<i class="icon-bin"></i>',
        indicatorNew: '<i class="icon-file-plus text-success"></i>',
        indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
        indicatorError: '<i class="icon-cross2 text-danger"></i>',
        indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
    },

    fileInput: (id) => {
        const t = this;

        $(`#${id}`).fileinput({
            showUpload: false,
            browseLabel: 'Browse',
            browseIcon: '<i class="icon-file-plus mr-2"></i>',
            uploadIcon: '<i class="icon-file-upload2 mr-2"></i>',
            removeIcon: '<i class="icon-cross2 font-size-base mr-2"></i>',
            layoutTemplates: {
                icon: '<i class="icon-file-check"></i>',
                modal: t.modalTemplate
            },
            initialCaption: "No file selected",
            previewZoomButtonClasses: t.previewZoomButtonClasses,
            previewZoomButtonIcons: t.previewZoomButtonIcons,
            fileActionSettings: t.fileActionSettings
        });
    },

    fileInputInitialPreview : (id, urlList) => {
        if (!Array.isArray) {
            console.error("fileInputInitialPreview, urlList is not array");
            return;
        }

        $(`#${id}`).fileinput({
            browseLabel: 'Browse',
            browseIcon: '<i class="icon-file-plus mr-2"></i>',
            uploadIcon: '<i class="icon-file-upload2 mr-2"></i>',
            removeIcon: '<i class="icon-cross2 font-size-base mr-2"></i>',
            layoutTemplates: {
                icon: '<i class="icon-file-check"></i>',
                modal: this.modalTemplate
            },
            initialPreview: [
                '../template/global_assets/images/placeholders/placeholder.jpg',
                '../template/global_assets/images/placeholders/placeholder.jpg',
            ],
            initialPreviewConfig: [
                { caption: 'Jane.jpg', size: 930321, key: 1, url: '{$url}', showDrag: false },
                { caption: 'Anna.jpg', size: 1218822, key: 2, url: '{$url}', showDrag: false }
            ],
            initialPreviewAsData: true,
            overwriteInitial: true,
            previewZoomButtonClasses: this.previewZoomButtonClasses,
            previewZoomButtonIcons: this.previewZoomButtonIcons,
            fileActionSettings: this.fileActionSettings
        });
    }
}