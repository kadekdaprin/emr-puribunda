﻿using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmrPuriBunda.Mahas.Helpers
{
    public static class CookiesHelper
    {
        public static void PutSection(this HttpResponse response, mSectionLogin mSection)
        {
            var cookieOption = new CookieOptions()
            {
                Path = "/",
                HttpOnly = false,
                Secure = false,
                Expires = DateTimeOffset.MaxValue,
                IsEssential = true
            };

            response.Cookies.Delete("SectionId");
            response.Cookies.Append("SectionId", mSection.SectionID, cookieOption);

            response.Cookies.Delete("SectionName");
            response.Cookies.Append("SectionName", mSection.SectionName, cookieOption);

            response.Cookies.Delete("GroupPelayanan");
            response.Cookies.Append("GroupPelayanan", mSection.GroupPelayanan, cookieOption);

            response.Cookies.Delete("EResepSectionID");
            response.Cookies.Append("EResepSectionID", mSection.EResepSectionID ?? "", cookieOption);

            response.Cookies.Delete("EResepSectionName");
            response.Cookies.Append("EResepSectionName", mSection.EResepSectionName ?? "", cookieOption);
    }

        public static void ClearSection(this HttpResponse response)
        {
            response.Cookies.Delete("SectionId");
            response.Cookies.Delete("SectionName");
            response.Cookies.Delete("GroupPelayanan");
            response.Cookies.Delete("EResepSectionID");
            response.Cookies.Delete("EResepSectionName");
        }

        public static mSectionLogin GetSection(this HttpRequest request)
        {
            return new mSectionLogin()
            {
                SectionID = request.Cookies["SectionId"].ToString(),
                SectionName = request.Cookies["SectionName"].ToString(),
                GroupPelayanan = request.Cookies["GroupPelayanan"].ToString(),
                EResepSectionID = request.Cookies["EResepSectionID"].ToString(),
                EResepSectionName = request.Cookies["EResepSectionName"].ToString()
            };
        }

        public static void PutDokter(this HttpResponse response, DokterModel dokter)
        {
            var cookieOption = new CookieOptions()
            {
                Path = "/",
                HttpOnly = false,
                Secure = false,
                Expires = DateTimeOffset.MaxValue,
                IsEssential = true
            };

            response.Cookies.Delete("DokterID");
            response.Cookies.Delete("NamaDokter");

            if(dokter != null)
            {
                response.Cookies.Append("DokterID", dokter.DokterID, cookieOption);
                response.Cookies.Append("NamaDokter", dokter.NamaDOkter, cookieOption);
            }
        }

        public static DokterModel GetDokter(this HttpRequest request)
        {
            return new DokterModel()
            {
                DokterID = request.Cookies["DokterID"].ToString(),
                NamaDOkter = request.Cookies["NamaDOkter"].ToString(),
            };
        }
    }
}