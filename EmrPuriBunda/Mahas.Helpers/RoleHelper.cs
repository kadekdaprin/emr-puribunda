﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static EmrPuriBunda.Constant.RolesConstant;

namespace EmrPuriBunda.Mahas.Helpers
{
    public static class RoleHelper
    {
        public static bool IsRoleDokterSpesialis(this ClaimsPrincipal user)
        {
            return user.IsInRole(DokterSpesialis)
                || user.IsInRole(DokterSpAnak)
                || user.IsInRole(DokterSpObgyn)
                || user.IsInRole(DokterSpPD)
                || user.IsInRole(DokterSpAnestesi)
                || user.IsInRole(DokterSpBedah)
                || user.IsInRole(DokterSpBedahAnak);
        }

        public static bool IsRolePerawatBidan(this ClaimsPrincipal user)
        {
            return user.IsInRole(PerawatBidan)
                || user.IsInRole(CaseManager);
        }

        public static bool IsRoleAdmin(this ClaimsPrincipal user)
        {
            return user.IsInRole(Admin);
        }
    }
}
