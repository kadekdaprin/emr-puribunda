﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static EmrPuriBunda.Constant.PolicyConstant;
using static EmrPuriBunda.Constant.RolesConstant;
using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace EmrPuriBunda.Mahas.Helpers
{
    public class DenyAnonymousAuthorizationRequirementOverrideOthers : DenyAnonymousAuthorizationRequirement { }

    public static class PolicyHelper
    {

        public static void ConfigurePolicy(this AuthorizationOptions config)
        {
            config.AddPolicy(SetupMaster, policy =>
            {
                policy.RequireRole(Admin, Kabid);
            });

            config.AddPolicy(AsesmenAwalDokterRJ, policy =>
            {
                policy.RequireRole(Admin, DokterSpesialis, DokterSpAnak, DokterSpObgyn, DokterSpPD, DokterSpAnestesi, DokterSpBedah, DokterSpBedahAnak, Dod, RekamMedis, Casemic);
            });

            config.AddPolicy(AsesmenAwalDokterIGD, policy =>
            {
                policy.RequireRole(Admin, DokterSpesialis, DokterSpAnak, DokterSpObgyn, DokterSpPD, DokterSpAnestesi, DokterSpBedah, DokterSpBedahAnak, Dod, RekamMedis, Casemic);
            });

            config.AddPolicy(AsesmenAwalDokterRI, policy =>
            {
                policy.RequireRole(Admin, DokterSpesialis, DokterSpAnak, DokterSpObgyn, DokterSpPD, DokterSpAnestesi, DokterSpBedah, DokterSpBedahAnak, Dod, RekamMedis, Casemic);
            });

            config.AddPolicy(AsesmenAwalPerawatRJ, policy =>
            {
                policy.RequireRole(Admin, PerawatBidan, CaseManager, RekamMedis, Casemic);
            });

            config.AddPolicy(AsesmenAwalPerawatIGD, policy =>
            {
                policy.RequireRole(Admin, PerawatBidan, CaseManager, RekamMedis, Casemic);
            });

            config.AddPolicy(AsesmenAwalPerawatRI, policy =>
            {
                policy.RequireRole(Admin, PerawatBidan, CaseManager, RekamMedis, Casemic);
            });

            config.AddPolicy(DischargeSummary, policy =>
            {
                policy.RequireRole(Admin, DokterSpesialis, DokterSpAnak, DokterSpObgyn, DokterSpPD, DokterSpAnestesi, DokterSpBedah, DokterSpBedahAnak, PerawatBidan, CaseManager, RekamMedis, Casemic);
            });

            config.AddPolicy(Cppt, policy =>
            {
                policy.RequireRole(Admin, DokterSpesialis, DokterSpAnak, DokterSpObgyn, DokterSpPD, DokterSpAnestesi, DokterSpBedah, DokterSpBedahAnak, Dod, PerawatBidan, CaseManager, Farmasi, AhliGizi, RekamMedis, Casemic, Psikolog, Terapis, Apoteker);
            });

            config.AddPolicy(Riwayat, policy =>
            {
                policy.RequireRole(Admin, DokterSpesialis, DokterSpAnak, DokterSpObgyn, DokterSpPD, DokterSpAnestesi, DokterSpBedah, DokterSpBedahAnak, Dod, PerawatBidan, CaseManager, Farmasi, AhliGizi, RekamMedis, Casemic, Manajemen, Kabid, Psikolog, Terapis, Apoteker);
            });

            config.AddPolicy(EResep, policy =>
            {
                policy.RequireRole(Admin, DokterSpesialis, DokterSpAnak, DokterSpObgyn, DokterSpPD, DokterSpAnestesi, DokterSpBedah, DokterSpBedahAnak, Dod, PerawatBidan, CaseManager, RekamMedis, Casemic);
            });

            config.AddPolicy(EPenunjang, policy =>
            {
                policy.RequireRole(Admin, DokterSpesialis, DokterSpAnak, DokterSpObgyn, DokterSpPD, DokterSpAnestesi, DokterSpBedah, DokterSpBedahAnak, Dod, PerawatBidan, CaseManager);
            });

            config.AddPolicy(InputPenunjang, policy =>
            {
                policy.RequireRole(Admin, Laboran, Radiografer, Kabid, PerawatBidan, CaseManager) ;
            });

            config.AddPolicy(InputEDokumen, policy =>
            {
                policy.RequireRole(Admin, RekamMedis, Casemic, Kabid);
            });

            config.AddPolicy(SadurAsesmen, policy =>
            {
                policy.RequireRole(Admin, DokterSpesialis, DokterSpAnak, DokterSpObgyn, DokterSpPD, DokterSpAnestesi, DokterSpBedah, DokterSpBedahAnak, Dod, PerawatBidan, CaseManager, RekamMedis, Casemic);
            });
        }

        public static async Task<bool> IsInPolicyAsync(this IAuthorizationService authService, ClaimsPrincipal user, string policy)
        {
            var result = await authService.AuthorizeAsync(user, policy);

            return result.Succeeded;
        }
    }
}
