﻿using EmrPuriBunda.FileManager.Models;
using EmrPuriBunda.Mahas.Helpers;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Serializers.NewtonsoftJson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace EmrPuriBunda.FileManager
{
    public class FileManagerService
    {
        private readonly RestClient restClient;
        private readonly AppSettings appSettings;

        public FileManagerService(IOptions<AppSettings> options)
        {
            appSettings = options.Value;
            restClient = new RestClient(appSettings.BaseUrlFileManager);
        }

        public FileManagerService()
        {
            restClient = new RestClient(appSettings.BaseUrlFileManager);

            restClient.UseNewtonsoftJson(new JsonSerializerSettings()
            {
                DateFormatHandling = DateFormatHandling.IsoDateFormat
            });
        }

        public byte[] GetDokumenRekamMedis(string pathDokumen)
        {
            var request = new RestRequest("RekamMedis", Method.GET);

            request.AddParameter("pathDokumen", pathDokumen);

            try
            {
                var data = restClient.DownloadData(request);
                return data;
            }
            catch 
            {
                throw new Exception("File tidak ditemukan");
            }

        }

        public async Task<List<DokumenRekamMedis_DokumenRekamMedisDetail_SetupModel>> PostDokumenRekamMedisAsync(DokumenRekamMedis_DokumenRekamMedis_SetupModel model, ICollection<IFormFile> FileList, string jenisKerjasama, List<string> deletePath = null, int startNoUrut = 0)
        {
            deletePath ??= new List<string>();
            var request = new RestRequest("RekamMedis", Method.POST);

            request.AlwaysMultipartFormData = true;

            request.AddParameter("NoBukti", model.NoBukti);
            request.AddParameter("NRM", model.NRM);
            request.AddParameter("TglReg", model.TglReg.GetValueOrDefault().ToString("yyyy-MM-dd"));
            request.AddParameter("RawatInap", model.RawatInap);
            request.AddParameter("JenisKerjasama", jenisKerjasama);
            request.AddParameter("ArsipClaimBpjs", model.ArsipClaimBpjs);
            request.AddParameter("startNoUrut", startNoUrut);

            foreach (var path in deletePath)
            {
                request.AddParameter("deletePath", path);
            }

            foreach (var file in FileList)
            {
                using (var ms = new MemoryStream())
                {
                    file.CopyTo(ms);
                    var fileBytes = ms.ToArray();

                    request.AddFile("FileList", fileBytes, file.FileName, file.ContentType);
                }
            }

            var response = await restClient.ExecuteAsync<List<DokumenRekamMedis_DokumenRekamMedisDetail_SetupModel>>(request);

            if (!response.IsSuccessful)
            {
                var errorResponse = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);

                var message = errorResponse?.Message ?? response.ErrorMessage;

                throw new Exception(message);
            }

            return response.Data;
        }

        public async Task DeleteDokumenRekamMedisAsync(string pathDokumen)
        {
            var request = new RestRequest("RekamMedis", Method.DELETE);

            request.AddParameter("pathDokumen", pathDokumen);

            var response = await restClient.ExecuteAsync(request);

            if (!response.IsSuccessful)
            {
                var errorResponse = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);

                var message = errorResponse?.Message ?? response.ErrorMessage;

                throw new Exception(message);
            }
        }

        public byte[] GetDokumenPenunjang(string pathDokumen)
        {
            var request = new RestRequest("Penunjang", Method.GET);

            request.AddParameter("pathDokumen", pathDokumen);

            var response = restClient.DownloadData(request);

            return response;
        }

        public async Task<List<InputHasilPenunjang_HasilBacaPenunjang_tablePathDokumen_SetupModel>> PostDokumenPenunjangAsync(InputHasilPenunjang_HasilBacaPenunjang_SetupModel model, ICollection<IFormFile> FileList, List<string> deletePath = null, int startNoUrut = 0)
        {
            deletePath ??= new List<string>();
            var request = new RestRequest("Penunjang", Method.POST);

            request.AlwaysMultipartFormData = true;

            request.AddParameter("NoBukti", model.NoBukti);
            request.AddParameter("NRM", model.NRM);
            request.AddParameter("SectionID", model.SectionID);
            request.AddParameter("startNoUrut", startNoUrut);


            foreach (var path in deletePath)
            {
                request.AddParameter("deletePath", path);
            }

            foreach (var file in FileList)
            {
                using (var ms = new MemoryStream())
                {
                    file.CopyTo(ms);
                    var fileBytes = ms.ToArray();

                    request.AddFile("FileList", fileBytes, file.FileName, file.ContentType);
                }
            }

            var response = await restClient.ExecuteAsync<List<InputHasilPenunjang_HasilBacaPenunjang_tablePathDokumen_SetupModel>>(request);

            if (!response.IsSuccessful)
            {
                var errorResponse = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);

                var message = errorResponse?.Message ?? response.ErrorMessage;

                throw new Exception(message);
            }

            return response.Data;
        }

        public async Task DeleteDokumenPenunjangAsync(string pathDokumen)
        {
            var request = new RestRequest("Penunjang", Method.DELETE);

            request.AddParameter("pathDokumen", pathDokumen);

            var response = await restClient.ExecuteAsync(request);

            if (!response.IsSuccessful)
            {
                var errorResponse = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);

                var message = errorResponse?.Message ?? response.ErrorMessage;

                throw new Exception(message);
            }
        }
    }
}
