﻿using EmrPuriBunda.Models;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmrPuriBunda.ViewComponents
{
    public class OrderPenunjangViewComponent : ViewComponent
    {
        private IConfiguration Config { get; set; }

        public OrderPenunjangViewComponent(IConfiguration configuration) => Config = configuration;

        public async Task<IViewComponentResult> InvokeAsync()
        {
            List<OrderPenunjang_OrderPenunjang_JookupJasa> datas;

            using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
            {
                var parameters = new List<SqlParameter>();
                var wheres = new List<string>();
                var _orderBy = new List<string>()
                    {
                        "UrutJenis",
                        "NamaJenis",
                        "UrutKelompok",
                        "NamaKelompok",
                        "UrutJasa",
                        "NamaTindakan"
                    };

                var query = $@"
                        SELECT
                            *
                        FROM
                            OrderPenunjang_LookupJasa
                        ORDER BY
                            UrutJenis,
                            UrutKelompok,
                            UrutJasa
                    ";

                datas = await s.GetDatas<OrderPenunjang_OrderPenunjang_JookupJasa>(query, parameters);
            }

            var kelompokList = datas.GroupBy(x => new { x.UrutJenis, x.NamaJenis, x.UrutKelompok, x.NamaKelompok, x.IDKelompok, x.SectionID })
                .Select(y => new OrderPenunjang_OrderPenunjang_Kelompok()
                {
                    NamaKelompok = y.Key.NamaKelompok,
                    UrutKelompok = y.Key.UrutKelompok,
                    NamaJenis = y.Key.NamaJenis,
                    UrutJenis = y.Key.UrutJenis,
                    IDKelompok = y.Key.IDKelompok,
                    SectionID = y.Key.SectionID,
                    Tindakan = y.Select(z => new OrderPenunjang_OrderPenunjang_Tindakan()
                    {
                        NamaTindakan = z.NamaTindakan,
                        UrutJasa = z.UrutJasa,
                        JasaID = z.JasaID,
                        IDKelompok = z.IDKelompok
                    }).ToList()
                });

            var jenisList = kelompokList.GroupBy(x => new { x.NamaJenis, x.UrutJenis, x.SectionID })
                .Select(y => new OrderPenunjang_OrderPenunjang_Jenis()
                {
                    SectionID = y.Key.SectionID,
                    NamaJenis = y.Key.NamaJenis,
                    UrutJenis = y.Key.UrutJenis,
                    Kelompok = y.ToList()
                });

            var model = new OrderPenunjang_OrderPenunjang_ViewModel()
            {
                ItemList = jenisList.ToList()
            };

            return View(model);
        }
    }
}