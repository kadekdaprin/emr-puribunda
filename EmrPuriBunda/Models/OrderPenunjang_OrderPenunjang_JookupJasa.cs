using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("EMR_OrderPenunjang")]
    public class OrderPenunjang_OrderPenunjang_ListModel
    {
        [DbColumn]
        public string NoBukti { get; set; }

        [DbColumn]
        public string Noreg { get; set; }

        [DbColumn]
        public string NamaPasien { get; set; }

        [DbColumn]
        public bool? Hasil_Cito { get; set; }

        [DbColumn]
        public bool? Hasil_Rutin { get; set; }

        [DbColumn]
        public string KodeICD { get; set; }

        [DbColumn]
        public string DiagnosaIndikasi { get; set; }

        [DbColumn]
        public string DokterID { get; set; }

        [DbColumn]
        public string NamaVendor { get; set; }

        [DbColumn]
        public string AlamatVendor { get; set; }

        [DbColumn]
        public string TlpVendor { get; set; }

        [DbColumn]
        public DateTime? DiterimaTgl { get; set; }

        [DbColumn]
        public DateTime? JamSampling { get; set; }

        [DbColumn]
        public DateTime? JamBilling { get; set; }

        [DbColumn]
        public string KodePetugas { get; set; }

        [DbColumn]
        public string NamaPetugas { get; set; }

        [DbColumn]
        public string PemeriksaanTambahan { get; set; }

        [DbColumn]
        public int? Realisasi { get; set; }

        [DbColumn]
        public DateTime? Tanggal { get; set; }

        [DbColumn]
        public bool? Batal { get; set; }

        public string OrderPenunjang { get; set; }

        public string CatatanPemeriksaan { get; set; }
    }

    [DbTable("OrderPenunjang_LookupJasa")]
    public class OrderPenunjang_OrderPenunjang_JookupJasa
    {
        [DbColumn]
        public string SectionID { get; set; }

        [DbColumn]
        public int? UrutJenis { get; set; }

        [DbColumn]
        public string NamaJenis { get; set; }

        [DbColumn]
        public int? UrutKelompok { get; set; }

        [DbColumn]
        public int IDKelompok { get; set; }

        [DbColumn]
        public string NamaKelompok { get; set; }

        [DbColumn]
        public int? UrutJasa { get; set; }

        [DbColumn]
        public string NamaTindakan { get; set; }

        [DbColumn]
        public string JasaID { get; set; }
    }

    public class OrderPenunjang_OrderPenunjang_Tindakan
    {
        public int? UrutJasa { get; set; }

        public string NamaTindakan { get; set; }

        public string JasaID { get; set; }

        public int IDKelompok { get; set; }

    }

    public class OrderPenunjang_OrderPenunjang_Kelompok
    {
        public string SectionID { get; set; }

        public int? UrutJenis { get; set; }

        public string NamaJenis { get; set; }
        public int IDKelompok { get; set; }

        public string NamaKelompok { get; set; }

        public int? UrutKelompok { get; set; }

        public int JumlahTindakan { get => Tindakan?.Count ?? 0; }

        public List<OrderPenunjang_OrderPenunjang_Tindakan> Tindakan { get; set; }
    }

    public class OrderPenunjang_OrderPenunjang_Jenis
    {
        public string SectionID { get; set; }

        public int? UrutJenis { get; set; }

        public string NamaJenis { get; set; }

        public int JumlahKelompok { get => Kelompok?.Count ?? 0; }

        public List<OrderPenunjang_OrderPenunjang_Kelompok> Kelompok { get; set; }
    }

    public class OrderPenunjang_OrderPenunjang_ViewModel
    {
        public List<OrderPenunjang_OrderPenunjang_Jenis> ItemList { get; set; }
    }
}