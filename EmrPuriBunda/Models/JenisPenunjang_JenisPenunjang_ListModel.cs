using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mJenisPenunjang")]	public class JenisPenunjang_JenisPenunjang_ListModel	{		[DbKey(true)]		[DbColumn]		public int Id { get; set; }		[DbColumn]		public string JenisPenunjang { get; set; }	}
}