using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    public class LaporanKelengkapanPengisianAssesmenAwalMedis_ListModel
    {
        public string NoReg { get; set; }

        public string NRM { get; set; }

        public string SectionName { get; set; }

        public string NamaDOkter { get; set; }

        public int? S { get; set; }

        public int? O { get; set; }

        public int? A { get; set; }

        public int? P { get; set; }

        public int? StatusLengkap { get; set; }

        public string JenisPelayanan { get; set; }

        public string SectionId { get; set; }

        public int Nomor { get; set; }
    }
}