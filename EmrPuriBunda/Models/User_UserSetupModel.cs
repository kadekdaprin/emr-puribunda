using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("AspNetUsers")]	public class User_UserSetupModel	{		[DbKey]		[DbColumn]		public string Id { get; set; }		[DbColumn]		public string UserName { get; set; }		[DbColumn]		public string NormalizedUserName { get; set; }		[DbColumn]		public string Email { get; set; }		[DbColumn]		public string NormalizedEmail { get; set; }		[DbColumn]		public bool EmailConfirmed { get; set; }		[DbColumn]		public string PasswordHash { get; set; }		[DbColumn]		public string SecurityStamp { get; set; }		[DbColumn]		public string ConcurrencyStamp { get; set; }		[DbColumn]		public string PhoneNumber { get; set; }		[DbColumn]		public bool PhoneNumberConfirmed { get; set; }		[DbColumn]		public bool TwoFactorEnabled { get; set; }		[DbColumn]		public DateTime? LockoutEnd { get; set; }		[DbColumn]		public bool LockoutEnabled { get; set; }		[DbColumn]		public int AccessFailedCount { get; set; }		public List<User_UserRoles> roles { get; set; }

        public string Password { get; set; }

		public string UlangiPassword { get; set; }

		public string IdProfesi { get; set; }
		public string Profesi { get; set; }

	}	[DbTable("AspNetUserRoles")]	public class User_UserRoles	{		[DbKey]		[DbColumn]		public string UserId { get; set; }		[DbKey]		[DbColumn]		public string RoleId { get; set; }		public string RoleName { get; set; }	}

	[DbTable("AspNetUsers")]	public class User_UserSetupModel_Update	{		[DbKey]		[DbColumn]		public string Id { get; set; }		[DbColumn]		public string UserName { get; set; }		[DbColumn(create:false, update:true)]		public string NormalizedUserName { get; set; }		[DbColumn]		public string Email { get; set; }

        public string IdProfesi { get; set; }

		public string Profesi { get; set; }

		public List<User_UserRoles> roles { get; set; }	}
	public class User_PasswordReset
	{
        public string UserId { get; set; }
    }

}