using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mKeadaanSaatDatang")]	public class KeadaanSaatDatang_KeadaanSaatDatangModel	{		[DbColumn]		public byte Id { get; set; }		[DbColumn]		public string KeadaanSaatDatang { get; set; }	}
}