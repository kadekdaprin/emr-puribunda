﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmrPuriBunda.Models
{
    public class Pasien_PasienRegistrasiModel
    {
        public string NoReg { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKerjasama { get; set; }
        public string TempatLahir { get; set; }
        public DateTime? TglLahir { get; set; }
        public string Alamat { get; set; }
        public string NoIdentitas { get; set; }
        public string JenisKelamin { get; set; }
        public double? BeratBadan { get; set; }
        public string StatusBayar { get; set; }

        public string Phone { get; set; }
        public string Agama { get; set; }
        public string Pendidikan { get; set; }
        public string Etnis { get; set; }
        public string NamaAlias { get; set; }

        public string Pekerjaan { get; set; }
        public string HambatanBerkomunikasi { get; set; }
        public bool? PasienVVIP { get; set; }
        public string PenanggungNama { get; set; }
        public string PenanggungTelp { get; set; }
        public string PenanggungHubungan { get; set; }
        public string NamaPerusahaan { get; set; }

        public string UmurTahun
        {
            get
            {
                if (TglLahir == null) return "? tahun";

                var umurth = Umur.IndexOf("tahun");

                return Umur.Substring(0, 0 + umurth) + "tahun";
            }
        }

        public string Umur
        {
            get
            {
                if (TglLahir == null) return "";

                var dob = TglLahir.GetValueOrDefault();

                var today = DateTime.Today;

                // Calculate the age.
                int months = today.Month - dob.Month;

                int years = today.Year - dob.Year;

                if (today.Day < dob.Day)
                {
                    months--;
                }

                if (months < 0)
                {
                    years--;
                    months += 12;
                }

                int days = (today - dob.AddMonths((years * 12) + months)).Days;

                return $"{years} tahun, {months} bulan, {days} hari";
            }
        }
    }

    public class Pasien_PengisianEMR
    {
        public string NoReg { get; set; }

        public bool AsesmenDokter { get; set; }

        public bool AsesmenPerawat { get; set; }

        public bool DischargeSummary { get; set; }
    }
}
