﻿using Mahas.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmrPuriBunda.Models
{
	[DbTable("Vw_trDokumenRekamMedis")]
	public class DokumenRekamMedis_Header_ListModel
	{
		[DbColumn]
		public string NoBukti { get; set; }

		[DbColumn]
		public DateTime? TanggalDibuat { get; set; }

		[DbColumn]
		public string NoReg { get; set; }

		[DbColumn]
		public string NRM { get; set; }

		[DbColumn]
		public DateTime? TglReg { get; set; }

		[DbColumn]
		public int? JenisKerjasamaID { get; set; }

		[DbColumn]
		public string SectionID { get; set; }

		[DbColumn]
		public string UserID { get; set; }

		[DbColumn]
		public bool? RawatJalan { get; set; }

		[DbColumn]
		public bool? RawatInap { get; set; }

		[DbColumn]
		public string Catatan { get; set; }

		[DbColumn]
		public string NamaPasien { get; set; }

		[DbColumn]
		public string JenisKelamin { get; set; }

		[DbColumn]
		public string Alamat { get; set; }

		[DbColumn]
		public DateTime? TglLahir { get; set; }

		[DbColumn]
		public string JenisKerjasama { get; set; }

		[DbColumn]
		public string SectionName { get; set; }
	}
}
