﻿using Mahas.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmrPuriBunda.Models
{
    [DbTable("trRiwayatAlergiPasien")]
    public class RiwayatAlergiPasien_SetupModel
    {
        [DbKey(true)]
        [DbColumn]
        public int ID { get; set; }
        
        [DbColumn]
        public DateTime? Tanggal {get; set;}
        
        [DbColumn]
        public string NRM {get; set;}
        
        [DbColumn]
        public string RiwayatAlergi   {get; set;}
        
        [DbColumn]
        public int? JenisID {get; set;}
        
        [DbColumn]
        public string KeteranganAlergi {get; set;}
    }

    public class RiwayatAlergiGabung
    {
        public string RiwayatAlergi { get; set; }
    }
}
