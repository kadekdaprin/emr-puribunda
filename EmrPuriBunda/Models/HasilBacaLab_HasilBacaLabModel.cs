using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("EMR_VIEW_LABORATORIUM")]	public class HasilBacaLab_HasilBacaLabModel	{		[DbColumn]		public string NoSystem { get; set; }		[DbColumn]		public string RegNo { get; set; }		[DbColumn]		public DateTime? Tanggal { get; set; }		[DbColumn]		public string DokterPengirim { get; set; }		[DbColumn]		public string SectionAsal { get; set; }		[DbColumn]		public string PenanggungJawab { get; set; }		[DbColumn]		public string Analis { get; set; }		[DbColumn]		public string Pemeriksaan { get; set; }

		[DbColumn]
		public string SourceData { get; set; }

		[DbColumn]
		public string SectionID { get; set; }

		[DbColumn]
		public int? CriticalValue { get; set; }
		
		[DbColumn]
		public int? Dibaca  {get; set;}
		
		[DbColumn]
		public string Komentar    {get; set;}
	}
}