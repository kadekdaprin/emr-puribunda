using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{	public class HasilBacaLab_HasilBacaLabSetupModel	{		public string NoSystem { get; set; }		public string RegNo { get; set; }		public DateTime? Tanggal { get; set; }		public string DokterPengirim { get; set; }		public string SectionName { get; set; }		public string PenanggungJawab { get; set; }		public string Analis { get; set; }	}
}