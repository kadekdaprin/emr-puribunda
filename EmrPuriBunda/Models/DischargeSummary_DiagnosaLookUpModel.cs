﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmrPuriBunda.Models
{
    public class DischargeSummary_DiagnosaLookUpModel
    {
        public string KodeICD { get; set; }

        public string SectionID { get; set; }

        public string Descriptions { get; set; }

        public string DiagnosaHC { get; set; }

        public string IDGroupICD { get; set; }
    }
}
