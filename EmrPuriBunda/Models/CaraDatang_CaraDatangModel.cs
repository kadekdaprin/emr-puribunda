using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mCaraDatang")]	public class CaraDatang_CaraDatangModel	{		[DbColumn]		public string CaraDatang { get; set; }		[DbKey(true)]		[DbColumn]		public int Id { get; set; }	}
}