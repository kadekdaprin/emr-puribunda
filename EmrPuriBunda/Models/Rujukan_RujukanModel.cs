using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mRujukan")]	public class Rujukan_RujukanModel	{		[DbKey(true)]		[DbColumn]		public int Id { get; set; }		[DbColumn]		public string Rujukan { get; set; }	}
}