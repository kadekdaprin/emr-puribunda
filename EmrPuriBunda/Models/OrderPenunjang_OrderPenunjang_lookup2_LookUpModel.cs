using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    public class OrderPenunjang_OrderPenunjang_lookup2_LookUpModel	{		public string DokterID { get; set; }		public string NamaDOkter { get; set; }		public string Alamat { get; set; }		public string NoKontak { get; set; }		public string SpesialisasiID { get; set; }		public string SubSpesialisasiID { get; set; }		public bool? Tetap { get; set; }		public double? THT { get; set; }		public double? HonorDefault { get; set; }		public double? KomisiDefault { get; set; }		public string IDPersonal { get; set; }		public string KodeKategoriVendor { get; set; }		public double? Pajak { get; set; }		public bool Active { get; set; }		public string SpesialisName { get; set; }		public string SubSpesialisName { get; set; }		public string Kode_Customer { get; set; }	}
}