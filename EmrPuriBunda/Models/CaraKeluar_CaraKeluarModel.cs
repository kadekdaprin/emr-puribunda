using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mCaraKeluar")]	public class CaraKeluar_CaraKeluarModel	{		[DbColumn]		public string CaraKeluar { get; set; }		[DbKey(true)]		[DbColumn]		public int Id { get; set; }	}
}