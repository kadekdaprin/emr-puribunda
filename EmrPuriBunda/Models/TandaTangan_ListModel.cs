using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("Vw_mProfesiTtd")]	public class TandaTangan_ListModel	{		[DbColumn]		public string DokterId { get; set; }		[DbColumn]		public byte[] TtdDokter { get; set; }		[DbColumn]		public string DokterId_Text { get; set; }	}
}