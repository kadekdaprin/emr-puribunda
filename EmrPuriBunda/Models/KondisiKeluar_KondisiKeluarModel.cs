using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mKondisiKeluar")]	public class KondisiKeluar_KondisiKeluarModel	{		[DbKey(true)]		[DbColumn]		public int Id { get; set; }		[DbColumn]		public string KondisiKeluar { get; set; }	}
}