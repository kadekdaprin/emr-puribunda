using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mProfesiTtd")]	public class TandaTangan_SetupModel	{		[DbKey]		[DbColumn]		public string DokterId { get; set; }		[DbColumn]		public byte[] TtdDokter { get; set; }		public string DokterId_Text { get; set; }	}
}