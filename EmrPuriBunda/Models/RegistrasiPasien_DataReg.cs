using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("VW_DataPasienReg")]	public class RegistrasiPasien_DataReg	{
        public string NoReg { get; set; }
        public DateTime Tanggal { get; set; }
        public DateTime Jam { get; set; }
        public int Nomor { get; set; }
        public int? NoAntri { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKerjasama { get; set; }
        public string TempatLahir { get; set; }
        public DateTime? TglLahir { get; set; }
        public string Alamat { get; set; }
        public string NoIdentitas { get; set; }
        public string JenisKelamin { get; set; }
        public double? BeratBadan { get; set; }
        public string StatusBayar { get; set; }

        public string Phone { get; set; }
        public string Agama { get; set; }
        public string Pendidikan { get; set; }
        public string Etnis { get; set; }
        public string NamaAlias { get; set; }

        public string Pekerjaan { get; set; }
        public string HambatanBerkomunikasi { get; set; }
        public bool? PasienVVIP { get; set; }
        public string PenanggungNama { get; set; }
        public string PenanggungTelp { get; set; }
        public string PenanggungHubungan { get; set; }
        public string NoKamar { get; set; }
        public string NamaPerusahaan { get; set; }

        public string NamaDokter { get; set; }
        public string DokterID { get; set; }

        public bool? SudahCheckout { get; set; }

        public bool? AsesmenRJ { get; set; }
        public bool? AsesmenRI { get; set; }
        public bool? AsesmenUGD { get; set; }

        public byte Triage { get; set; }

        public int? Disposisi { get; set; }

        public string UmurTahun
        {
            get
            {
                if (TglLahir == null) return "? tahun";

                var umurth = Umur.IndexOf("tahun");

                return Umur.Substring(0, 0 + umurth) + "tahun";
            }
        }

        public string Umur
        {
            get
            {
                if (TglLahir == null) return "";

                var dob = TglLahir.GetValueOrDefault();

                var today = DateTime.Today;

                // Calculate the age.
                int months = today.Month - dob.Month;

                int years = today.Year - dob.Year;

                if (today.Day < dob.Day)
                {
                    months--;
                }

                if (months < 0)
                {
                    years--;
                    months += 12;
                }

                int days = (today - dob.AddMonths((years * 12) + months)).Days;

                return $"{years} tahun, {months} bulan, {days} hari";
            }
        }
    }

    public class RegistrasiPasien_DataRegWithMenu
    {
        public RegistrasiPasien_DataReg Pasien { get; set; }
        public List<Pasien_MenuAkses> MenuAkses { get; set; }
    }
}