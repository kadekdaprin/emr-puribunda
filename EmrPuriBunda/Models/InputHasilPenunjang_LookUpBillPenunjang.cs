﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmrPuriBunda.Models
{
    public class InputHasilPenunjang_LookUpBillPenunjang
    {
        public string NoBillPenunjang { get; set; }
        public DateTime? TanggalPeriksa { get; set; }
        public string SectionID { get; set; }
        public string SectionAsalID { get; set; }
        public string DokterID { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string Alamat { get; set; }
        public string JenisKelamin { get; set; }
        public DateTime? TglLahir { get; set; }
        public string SectionAsalID_Text { get; set; }
        public string SectionID_Text { get; set; }
        public string DokterID_Text { get; set; }
        public string VendorRujukanID { get; set; }
        public string VendorRujukanID_Text { get; set; }
        public string Pemeriksaan { get; set; }
    }
}
