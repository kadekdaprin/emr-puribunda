using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mOrderPenunjang_Jenis")]	public class OrderPenunjangJenis_OrderPenunjangJenis_SetupModel	{		[DbKey(true)]		[DbColumn]		public int ID { get; set; }		[DbColumn]		public string SectionID { get; set; }		[DbColumn]		public int? Nomor { get; set; }		[DbColumn]		public string Nama { get; set; }		public string SectionID_Text { get; set; }	}
}