using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mJenisTemplateAsesmen")]	public class JenisTemplate_ListModel	{		[DbKey]		[DbColumn]		public int Id { get; set; }		[DbColumn]		public string JenisTemplateAsesmen { get; set; }	}
}