using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mCpptTipe")]	public class CpptTipe_CpptTipeModel	{		[DbColumn]		public string CpptTipe { get; set; }		[DbKey(true)]		[DbColumn]		public int Id { get; set; }	}
}