using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mHubunganPasien")]	public class HubunganPasien_HubunganPasienModel	{		[DbColumn]		public string HubunganPasien { get; set; }		[DbKey(true)]		[DbColumn]		public int Id { get; set; }	}
}