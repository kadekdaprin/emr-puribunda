using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mPenolongPersalinan")]	public class PenolongPersalinan_PenolongPersalinan_ListModel	{		[DbKey(true)]		[DbColumn]		public int Id { get; set; }		[DbColumn]		public string PenolongPersalinan { get; set; }		[DbColumn]		public bool? Aktif { get; set; }	}
}