using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace EmrPuriBunda.Models
{
    [DbTable("trHasilBacaPenunjangReview")]
    public class InputHasilPenunjang_KomentarPenunjang_SetupModel
    {
        [DbKey]
        [DbColumn]
        public string NoBukti { get; set; }

        [DbColumn]

        public bool? Dibaca { get; set; }

        [DbColumn]

        public string Komentar { get; set; }

        [DbColumn]
        public string DokterID { get; set; }
    }
}