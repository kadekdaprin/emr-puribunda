using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("Cppt")]	public class Cppt_CpptSetupModel	{		[DbKey(true)]		[DbColumn]		public int Id { get; set; }		[DbColumn]		public string NRM { get; set; }		public string Nomor { get; set; }		[DbColumn]		public string NoReg { get; set; }		[DbColumn]		public string SectionId { get; set; }		[DbColumn]		public DateTime? Tanggal { get; set; }		[DbColumn]		public string Dpjp { get; set; }		[DbColumn]		public string Subjective { get; set; }		[DbColumn]		public string Objective { get; set; }		[DbColumn]		public string Assesment { get; set; }		[DbColumn]		public string Planning { get; set; }		[DbColumn]		public string Instruksi { get; set; }		[DbColumn]		public DateTime? Keluar_Tanggal { get; set; }		[DbColumn]		public byte? Id_mKondisiKeluar { get; set; }		[DbColumn]		public byte? Id_mCaraKeluar { get; set; }		[DbColumn]		public byte? Id_mCpptTipe { get; set; }		[DbColumn]		public string Id_mProfesi { get; set; }

		[DbColumn]
        public DateTime? LastUpdate { get; set; }

		[DbColumn]
		public bool? Tulis { get; set; }
		[DbColumn]
		public bool? Baca { get; set; }
		[DbColumn]
		public bool? Verifikasi { get; set; }
		[DbColumn]
		public DateTime? Verifikasi_Tanggal { get; set; }
		[DbColumn]
		public bool? ButuhKonfirmasi { get; set; }
		[DbColumn]
		public bool? SudahKonfirmasi { get; set; }
		[DbColumn]
		public DateTime? Konfirmasi_Tanggal { get; set; }
		[DbColumn]
		public string Konfirmasi_Dpjp { get; set; }

		[DbColumn]
		public string UserId { get; set; }

		[DbColumn]		public bool? AdaPerubahan { get; set; }

		[DbColumn]
		public string SumberData { get; set; }

        public string Id_mCpptTipe_Text { get; set; }		public string Id_mCaraKeluar_Text { get; set; }		public string Id_mProfesi_Text { get; set; }		public string Id_mKondisiKeluar_Text { get; set; }		public string Dpjp_Text { get; set; }		public string Konfirmasi_Dpjp_Text { get; set; }	}
}