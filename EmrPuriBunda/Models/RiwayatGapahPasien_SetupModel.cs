using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("trRiwayatGapahPasien")]	public class RiwayatGapahPasien_SetupModel	{		[DbKey(true)]		[DbColumn]		public int ID { get; set; }		[DbColumn]		public DateTime? Tanggal { get; set; }		[DbColumn]		public string NRM { get; set; }		[DbColumn]		public int? G { get; set; }		[DbColumn]		public int? P { get; set; }		[DbColumn]		public int? A { get; set; }		[DbColumn]		public int? H { get; set; }		[DbColumn]		public DateTime? HPHT { get; set; }		[DbColumn]		public DateTime? TP { get; set; }		[DbColumn]		public DateTime? LastUpdate { get; set; }		[DbColumn]		public string AspnetUserID { get; set; }		[DbColumn]		public string Keterangan { get; set; }		[DbColumn]		public string Id_mGpah { get; set; }		public string Id_mGpah_Text { get; set; }		public List<RiwayatPersalinan_SetupModel> RiwayatPersalinan { get; set; }	}
}