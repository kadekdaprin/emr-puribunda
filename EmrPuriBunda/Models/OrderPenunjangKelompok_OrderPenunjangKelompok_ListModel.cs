using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("Vw_mOrderPenunjang_Kelompok")]	public class OrderPenunjangKelompok_OrderPenunjangKelompok_ListModel	{		[DbColumn]		public int ID { get; set; }		[DbColumn]		public int JenisID { get; set; }		[DbColumn]		public int? Nomor { get; set; }		[DbColumn]		public string Nama { get; set; }		[DbColumn]		public string JenisID_Text { get; set; }	}
}